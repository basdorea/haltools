bibtexparser==1.4.0
celery==5.2.7
Django==3.2
django-auth-ldap==4.5.0
django-bootstrap-themes==3.3.6
django-bootstrap3==21.1
lxml==4.9.0
mysqlclient==2.1.0
python-slugify==8.0.1
requests==2.26.0


# pyparsing==2.3.1
# pytz==2018.9
# unicodecsv==0.14.1
# urllib3==1.24.1
