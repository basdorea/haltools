#-*- coding: utf-8 -*-

from django.core.mail import send_mail, EmailMessage

def sendonemail(mail_obj,mail_msg, to_user):
    email = EmailMessage(mail_obj, mail_msg, to=[to_user])
    email.send()
