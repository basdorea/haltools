#!/usr/bin/python
#-*- coding: utf-8 -*-

#from __future__ import unicode_literals
import requests
import re

# lib XML
from lxml import etree

# autres
import csv
import sys

from create_xml2hal import createXml_sendHal, create_single_xml

#################################################
# script_list_to_hal.py
#
# Ce script en python 2.7 permet de rentrer des publis d'un auteur dans HAL a partir d'une liste formatee de type HCERES.
# Cet import se fait par l'API SWORD de HAL   https://api.archives-ouvertes.fr/docs/sword
#
# Principe : A partir des infos de la liste, des fichiers XML sont generes et envoyes par requete POST a une URL d'import
# 
# Necessite : un compte HAL permettant l'import, des infos sur l'auteur et sur le labo affilie, qq connaissances sur le depot sur HAL et son fonctionnement
# 
# Le script permet d'importer dans HAL des articles, des participations à des conférences, des ouvrages, des chapitres d'ouvrage et des posters
# en proposant des étapes de vérification (un fichier CSV et un depot en preproduction ); la difficulté étant que la liste soit bien formatée
# Les imports sont faits en donnant la propriete de la publi a l'auteur, le contributeur du depot etant forcement le compte qui depose
# Une methode est utilisee pour generer un XML pour les articles, confs , posters , chapitres d'ouvrage et ouvrages
# Le script principal recupere les donnes, cree le CSV et appelle cette methode
# Une verification est faite pour savoir si la publi existe deja dans HAL, si c'est le cas, pas d'import
# Deux variables booleennes permettent d'importer ou non sur une base de pré-prod et sur une base de prod
# Pour les imports, mettre ces variables a True (normalement, soit on importe sur preprod pour tester, soit en prod mais pas les deux)
# Attention, pour deposer en preprod, il est necessaire d'avoir un compte en preprod en plus du compte en prod
#
# La bonne pratique
# 
# LECTURE DE LA LISTE 
# En general, les listes sont formatées de cette facon : 
# numero-de-publi , tiret , auteurs separes par des virgules , titre entre guillements , nom de la conf/journal/... , une virgule, autres infos 
# separees par des virgules comprenant l'annee, le doi eventuellement, le nb de pages , le lieu de la conf, ...
#
# Exemples avec deux articles, une conf et un chapitre d'ouvrage:
# RI52 - HILL D., "Parallel Random Numbers, Simulation, Science and reproducibility". IEEE CISE  Computing in Science and Engineering, Invited Paper, Volume:17,  Issue: 4, 2015, pp. 66 – 71, ISSN :1521-9615  doi:10.1109/MCSE.2015.79. 
# RI51 - PASSERAT-PALMBACH J., CAUX J., SCHWEITZER P., SIREGARD P., HILL D., "Harnessing aspect oriented programming on GPU: application to warp-level parallelism (WLP)", International Journal of Computer Aided Engineering and Technology, Vol. 7, No. 2, 2015, pp. 158-175.
# CI97 - SCHWEITZER P., MAZEL C., CARLOGANU C., HILL D., "Inputs of Aspect Oriented Programming for the Profiling of C++ Parallel Applications on Manycore Platforms", Proceedings of the 2014 International Conference on High Performance Computing & Simulation, ACM/IEEE/IFIP – WIP, $Bologna Italie, IT, July 25th-29th, 2014, pp. 793-802.
# CO3 - HILL D., COQUILLARD P., "Ecological Modelling and Simulation", Handbook of Dynamic System Modeling, Paul Fishwick Editor, CRC Press, Chapter 29, pp. 29-1,18, 2007.
#
# Il sera sans doute necessaire de formater la liste en remplacant « par " , en remplacant les doubles espaces par un seul, modifier les noms de pays
# Afin de recuperer les villes et pays des conferences se trouvant un peu n'importe ou en fin de texte, j'ai choisi de fixer le caractere $ avant le nom de la ville
# 
# DONNEES RECUPEREES
# Les donnees sont recuperees ligne par ligne
# - Recup des infos du debut jusqu'au tiret, selon les lettres indiquees on retire le type de publi (RI ou RN -> article , CO -> chapitre d'ouvrage, O -> ouvrage, CI ou CN -> conference, P -> poster), on recupere aussi le numero de la publi
# - recup du tiret jusqu'au premier guillemet -> tous les auteurs separes par des virgules (dans l'ordre, nom prenom) qui sont mis dans une liste -> listauthors
# si un des noms correspond a celui de l'auteur donne en variable, on specifie le labo
# - recup du titre du depot entre les guillemets 
# - recup du texte du second guillemet jusqu'a la premiere virgule -> nom de la conf/journal , si cette valeur n'a pas ete renseignee par l'auteur (car n'a pas lieu d'etre) une valeur est quand meme recuperee mais non utilisee
# - A partir du reste de la ligne, on recupere le DOI en cherchant le texte "doi" jusqu'a la virgule suivante
# on recupere les villes et pays en cherchant du $ jusqu'au premier espace puis du premier espace jusqu'au second. Cela signifie que 
# les noms des pays doivent etre formates (en francais et sans espace , voir liste dans acronym_country.csv)
# le code du pays est retrouve a partir du fichier CSV acronym_country.csv lu en debut de script
# le nombre de pages est recupere a partir de l'element "pp." jusqu'a la prochaine virgule ou la fin de la ligne
# l'annee est recuperee par une Regex pour les annees allant de 1970 a 2029
#
# ETAPES :
# 1 - La liste est lue en plein texte et des informations en sont retirées
# 2 - Un fichier CSV est généré à partir de ces informations afin d'avoir un visuel plus propre sur les infos récupérées 
# 3 - Autant de fichiers XML sont générés que de publications avec comme nom le numero de la publi suivi de "publi.xml" -> P6publi.xml
# 4 - Si specifie, les imports des XML sont faits sur la base de préproduction et/ou la base de production
# 5 - Les resultats et erreurs éventuelles sont affichees en sortie
#
# VARIABLES :
# Certaines variables sont à modifier en début de script pour spécifier :
# - l'auteur des publis (prénom, nom, login et idHal)
# - les domaines de publication de l'auteur
# - le code du labo précédé de "struct-"
# - les infos du compte de dépôt (login et mot de passe)
# - un champ booléen pour déposer sur la base de préproduction (défaut = False)
# - un champ booléen pour déposer sur la base de production (défaut = False)
# - le nom du fichier texte contenant les publis a lire
#
# Pour info, une liste de correspondance des domaines se trouve dans le fichier list_domaines.txt
#################################################




########################################################################################################################################
########################################################################################################################################
########## SCRIPT PRINCIPAL
########################################################################################################################################
########################################################################################################################################

def script_text_2_hal (text_file, form_author, name_user, firstname_user, labo_auth_final, id_hal_user, login_user, listdomains, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod, single):
    '''
    take text with publications and some values in entry and parse the text to get info upon publications

    if the bool 'single' == False -> bibtex file has many publications, call createXml_sendHal (create XML and deposit) return a lis of results    
    if the bool 'single' == True -> bibtex file has one publication, call create_single_xml (post single XML no deposit) return the XML in string
    '''
    resultat = ""
    problemes_url = ""
    problemes_doublon = ""
    problemes_depot = ""
    depots = ""

    reponse_single_xml = ""

    list_pub_csv = []
    
    #list_acronym_country = get_list_acronym_country() 
    list_acronym_country = []
    with open('hal/countries.csv', 'rb') as csvfile:
        delim = str(':')
        quotech = str('|')
        list_countries = csv.reader(csvfile, delimiter=delim, quotechar=quotech)
        for row in list_countries:
            list_acronym_country.append((row[1],row[0]))

    for c in list_acronym_country :
        print "pays "+c[0] + " - acr "+c[1]
    
    
    # Lecture de la liste
    list_publis = text_file.splitlines()
    
    for publi in list_publis:

        pubmed = ""
        
        language = "en"
        # recup numero avant le -
        x = publi.find("-")#.decode('utf-8')
        numero = publi[0:x]
        print numero
        type_publi="TYPE"
        if ("RI") in numero :
            type_publi = "ART"
    
        if ("RN") in numero :
            type_publi = "ART"
            language = "fr"
    
        if ("O") in numero : # prend O, CO et DO
            type_publi = "OUV"
    
        if ("CO") in numero :
            type_publi = "COUV"
    
        if ("CI") in numero :
            type_publi = "COMM"
    
        if ("CN") in numero :
            type_publi = "COMM"
            language = "fr"
    
        if ("P") in numero :
            type_publi = "POSTER"
    
        # recup auteurs a la suite jusqu'a "
        publi = publi[x+1:]
        x = publi.find("\"")
        authors = publi[:x]
        authors = authors.strip()
        #print authors
        list_authors_mix = authors.split(",")
        listauthors = []
    
        # si champ vide, non pris en compte
        if list_authors_mix[-1].strip() == "" :
            del list_authors_mix[-1]
    
        for auth in list_authors_mix :
            auth = auth.strip()
            newauth = auth.split(" ")
            
            if (newauth[0] in form_author):
                listauthors.append((name_user.encode('utf-8'),firstname_user.encode('utf-8'),labo_auth_final))
            else :
                listauthors.append((newauth[0].encode('utf-8'),newauth[1].encode('utf-8'),""))
    
        #recup titre entre " et "
        publi = publi[x+1:]
        x = publi.find("\"")
        title = publi[:x]
        title = title.strip()
        title = title.encode('utf-8')
        #print title
    
        #recup conf, suppression de premiere virgule ou point
        publi = publi[x+1:]
        publi = publi.strip()
        if (publi[0] == ",") or (publi[0] == ".") :
            publi = publi[1:]
        x = publi.find(",")
        conf = publi[:x]
        conf = conf.strip()
        conf = conf.encode('utf-8')
        #print conf
    
        # reste
        rest_publi = publi[x+1:]
    
        editor_book = ""
        if (type_publi == "" ) :
            x = rest_publi.find(",")
            editor_book = rest_publi[:x]
            editor_book = editor_book.strip()
            editor_book = editor_book.encode('utf-8')
    
        # recup DOI
        doi_value = ""
        x = rest_publi.find("doi")
        doi_value = rest_publi[x:]
        doi_value = doi_value.strip()
        x = doi_value.find(" ")
        doi_value = doi_value[:x]
        doi_value = doi_value.strip()
        if (doi_value[-1:] == ".") or (doi_value[-1:] == ",") :
            doi_value = doi_value[:-1] 
        #print "doi_value :",doi_value
        if ("doi:" in doi_value):
            doi_value = doi_value[4:] 
        print "doi_value :",doi_value
    
        # recup Ville, Pays , Acron
        town = "Ville_inconnue"
        country = "Pays_inconnu"
        country_acr = "XX"

        # recup pays par NOM
        all_elements = rest_publi.split(",")
        prev_conf_elmt = ""
        for conf_elmt in all_elements :
            conf_elmt = conf_elmt.strip()
            #print "conf_elmt "+conf_elmt
            for acr_country in list_acronym_country : 
                if acr_country[0] == conf_elmt :
                    town = prev_conf_elmt
                    town = town.encode('utf-8')
                    country_acr = acr_country[1]
                    country = acr_country[0]
                    #print "match country ", town,  country, country_acr
            prev_conf_elmt = conf_elmt

        #print (" town country "+town +" "+ country+" "+country_acr)
    
        #################
        # RECUP Nb pages + volume
        #print rest_publi
        nb_pages = ""
        x = rest_publi.find("pp.")
        nb_pages = rest_publi[x:]
        x = nb_pages.find(",")
        nb_pages = nb_pages[3:x]
        nb_pages = nb_pages.replace(".","")
        nb_pages = nb_pages.replace(",","")
        nb_pages = nb_pages.strip()
        if len(nb_pages) < 3 :
            print "PAS DE PAGES"
        else :
            print nb_pages
        
        volume = ""
        x = rest_publi.find("Vol.")
        volume = rest_publi[x:]
        x = volume.find(",")
        volume = volume[4:x]
        volume = volume.replace(".","")
        volume = volume.replace(",","")
        volume = volume.strip()
        if len(volume) < 1 :
            print "PAS DE Volume"
        else :
            print volume    
        
    
    
        # YEAR recup par regex les nombres allant de 1970 a 2029 et ne commencant pas par un nb pour eviter de trouver 2005 ds 478520051906
        try : 
            year = re.findall('\D([1][9][7-9][0-9]|[2][0][0-2][0-9])', rest_publi)[0]
        except IndexError :
            year = ""
    

        if single == False :    
                #######################
                ### TEST EXISTING PUBLI
                #######################
            action_todo=""

            # Verification que la publi n'existe pas deja dans HAL
            #titleutf8 = title.encode("utf-8")
            title_low = title.lower()
            url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(title_low)
            #print url_request
            req = requests.get(url_request)
            #print req.status_code
    
            json = req.json()
        
    
            try : 
                result = json['response']['docs']
                
                if (len(result) == 1 ) :
                    # recup authors
                    all_auth = ""
                    try :
                        tous_authors = result[0]["authFullName_s"]
                        for auth in tous_authors:
                            print (auth)
                            all_auth = all_auth + auth+"-"
                    except KeyError, e :
                        print ("error print authors existing publi")
                    resultat = resultat + "num. "+numero+" - "+result[0]["uri_s"]+" - auteurs:"+all_auth+"<br/>"
                    action_todo = "E"
                if (len(result) > 1 ) :
                    problemes_doublon = problemes_doublon + "num. "+numero+" - URLS "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"<br/>"
                    action_todo = "2"
                if (len(result) == 0 ) :
                    print ("Depot sur HAL")
                    action_todo = "D"
                    result = False
                    if (type_publi=="ART") or (type_publi=="COMM") or (type_publi=="POSTER") or (type_publi=="OUV") or (type_publi=="COUV"):
                        result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod)
                    if result == True :
                        depots = depots + "num. "+numero+" au titre "+title.decode('utf-8')+" deposee dans HAL<br/>"
                    if result == False :
                        problemes_depot = problemes_depot + "num. "+numero+" au titre "+title.decode('utf-8')+" a un probleme de depot<br/>"
                print ("-----------")
            except KeyError :
                print ("PROBLEMES")
                action_todo = "P"
                problemes_url = problemes_url + "num. "+numero+" au titre "+title+"<br/>"
        
        
            list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))

        elif single == True :
            reponse_single_xml = create_single_xml(listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr, doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user) 
            #print "REPONSE_SINGLE_XML : "+ reponse_single_xml   


    cnt_total = len(list_pub_csv)
    print ("list_pub_csv length :",len(list_pub_csv))

    list_resultats=[]
    for pub in list_pub_csv :
        numero = str(pub[0])
        allauth = pub[1]
        allauth = allauth.replace("\n","")
        allauth = allauth.encode("utf-8") 
        title = pub[2]#.encode("utf-8") 
        conf = pub[3]#.encode("utf-8") 
        nb_pages = pub[4].encode("utf-8")
        volume =  str(pub[5]).encode("utf-8")
        year =  str(pub[6]).encode("utf-8")


        ville = pub[9]#.encode("utf-8")   
        pays = pub[10].encode("utf-8")

        list_resultats.append((numero,allauth,title,conf,nb_pages,volume,year,str(pub[7]),str(pub[8]),ville,pays,str(pub[11]),str(pub[12])))

    list_resultats.append(("RESULTATS","nombre de publis",str(cnt_total),"","","","","","","","","",""))
    list_resultats.append(("RESULTATS","publis deja presentes dans HAL",resultat,"","","","","","","","","",""))
    list_resultats.append(("RESULTATS","depots",depots,"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","problemes de depot",problemes_depot,"","","","","", "","","","",""))
    list_resultats.append(("RESULTATS","problemes de doublon",problemes_doublon,"","","","", "","","","","","")) 
    list_resultats.append(("RESULTATS","problemes url",problemes_url,"","","","","","","","","","")) 
            
            
    
    print ("RESULTATS existants")
    print (resultat)
    print ("------------------")
    print ("DEPOTS effectues")
    print (depots)
    print ("------------------")
    print ("PROBLEMES DEPOTS")
    print (problemes_depot)
    print ("------------------")
    print ("PROBLEMES DOUBLONS")
    print (problemes_doublon)
    print ("------------------")
    print ("PROBLEMES URL")
    print (problemes_url)
    

    if single == False :
        return list_resultats
    if single == True :
        return reponse_single_xml



