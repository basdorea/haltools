


# URLs SCIMAGO pour tous les les domaines informatique
dict_scimago = {
    "SCIMago-J-2022.csv":'https://www.scimagojr.com/journalrank.php?year=2022&type=j&out=xls',
    "SCIMago-J-2021.csv":'https://www.scimagojr.com/journalrank.php?year=2021&type=j&out=xls',
    "SCIMago-J-2020.csv":'https://www.scimagojr.com/journalrank.php?year=2020&type=j&out=xls',
    "SCIMago-J-2019.csv":'https://www.scimagojr.com/journalrank.php?year=2019&type=j&out=xls',
    "SCIMago-J-2018.csv":'https://www.scimagojr.com/journalrank.php?year=2018&type=j&out=xls',
    "SCIMago-J-2017.csv":'https://www.scimagojr.com/journalrank.php?year=2017&type=j&out=xls',
    "SCIMago-J-2016.csv":'https://www.scimagojr.com/journalrank.php?year=2016&type=j&out=xls',
    "SCIMago-J-2015.csv":'https://www.scimagojr.com/journalrank.php?year=2015&type=j&out=xls',
    "SCIMago-J-2014.csv":'https://www.scimagojr.com/journalrank.php?year=2014&type=j&out=xls',
    "SCIMago-J-2013.csv":'https://www.scimagojr.com/journalrank.php?year=2013&type=j&out=xls'
}


# URLs CORE pour les confs en informatique
dict_core = {
    "CORE-C-2023.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2023&sort=arank&page=1&do=Export",
    "CORE-C-2021.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2021&sort=arank&page=1&do=Export",
    "CORE-C-2020.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2020&sort=arank&page=1&do=Export",
    "CORE-C-2018.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2018&sort=arank&page=1&do=Export",
    "CORE-C-2017.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2017&sort=arank&page=1&do=Export",
    "CORE-C-2014.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2014&sort=arank&page=1&do=Export",
    "CORE-C-2013.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=CORE2013&sort=arank&page=1&do=Export",
    "CORE-C-2010.csv":"http://portal.core.edu.au/conf-ranks/?search=&by=all&source=ERA2010&sort=arank&page=1&do=Export"
}



