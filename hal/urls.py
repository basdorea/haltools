from django.conf.urls import include, url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^connexion$', views.connexion, name='connexion'),
    url(r'^deconnexion$', views.deconnexion, name='deconnexion'),
    url(r'^blog$', views.article_blog, name='blog'),
    url(r'^bibtex2hal$', views.bibtex2hal, name='bibtex2hal'),
    url(r'^bibformat_2hal/(?P<csvfile>[\w\-]+)$', views.bibformat_2hal, name='bibformat_2hal'),
    #url(r'^validatepost2hal$', views.validatepost2hal, name='validatepost2hal'),

    url(r'^verifhalconf_ranking$', views.verifhalconf_ranking, name='verifhalconf_ranking'),
    url(r'^verifhal_byuser/(?P<csvfile>[\w\-]+)$', views.verifhal_byuser, name='verifhal_byuser'),
    
    #url(r'^bibtexxml$', views.bibtexxml, name='bibtexxml'),
    url(r'^csv2hal$', views.csv2hal, name='csv2hal'),
    url(r'^valid_post_hal$', views.valid_post_hal, name='valid_post_hal'),

    url(r'^searchdatahal$', views.search_hal_data, name='search_hal_data'),
    
    #url(r'^text2hal$', views.text2hal, name='text2hal'),
    #url(r'^textxml$', views.textxml, name='textxml'),
    #url(r'^textmodif$', views.textmodif, name='textmodif'),
    #url(r'^text_format$', views.text_format, name='text_format'),
    url(r'^aide$', views.aide, name='aide'),
]

