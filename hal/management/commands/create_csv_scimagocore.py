####################################
# Script create CSV SCIMago
####################################
# SCIMAGO et CORE sont des agences de notation de journaux et conferences scientifiques
# SCIMAGO est generaliste et donne des notes Q1, Q2, Q3 et Q4 et ne note que les journaux
# https://www.scimagojr.com/
# CORE est specialise en informatique et donne des notes A*, A, B et C et note journaux et conferences
# http://portal.core.edu.au/conf-ranks/
#
# Ce script permet la creation de CSV contenant pour les journaux le titre et la note, et pour les conferences, le titre, l'acronyme et la note
# Principe : 
# - on telecharge des CSV a partir d'URL dans des fichiers
# - on parse les CSV en recuperant les infos souhaitees (titre et note ou titre, acronyme et note) dans une ou des listes
# - on reconstruit un CSV a partir de la ou les listes
#
# Pour SCIMAGO, les journaux sont recuperes par annee et 3 domaines sont analyses (donc 3 URL de telechargement par annee) Computer Science, Mats et Engineering
# Pour CORE, les conf sont recuperes par annee mais une seule base pour les journaux
####################################

from django.core.management.base import BaseCommand, CommandError
from haltools.settings  import BASE_DIR
from hal.dict_scimago_core import dict_scimago, dict_core
import requests
import csv
import os

###########
# VARIABLES
###########

path_csv = str(BASE_DIR+"/ranking/")
# Compteurs
cnt_scimago_jrn=0
cnt_core_conf=0
#cnt_core_jrn=0
cnt_problems_sautligne=0

###########
# FONCTIONS
###########

# delete the downloaded file from scimago or core
def delete_file(download_file):
    if os.path.isfile(download_file):
        os.remove(download_file)

def correspondance_SJR_HAL(sjr_journal) :
    """
    As HAL has not exactly the same name of journals than SJR, some modifs are put on the CSV files extracted form Scimago
    """
    equivhal_journal = sjr_journal
    equivhal_journal = equivhal_journal.replace('Journal of Experimental Algorithmics','ACM Journal of Experimental Algorithmics')
    equivhal_journal = equivhal_journal.replace('Concurrency and Computation Practice and Experience','Concurrency and Computation: Practice and Experience')
    equivhal_journal = equivhal_journal.replace('4OR','4OR: A Quarterly Journal of Operations Research')
    return equivhal_journal


# Creation des fichiers SCIMago contenant titre des journaux et notes pour les 3 domaines : computer science, math, et engineering
# Lecture 3 CSV telecharge de la meme annee envoyes en parametres 
# Enregistrement titre et note a partir des 3 CSV dans 3 listes : list_sci_com, list_sci_mat, list_sci_eng si note = Q1, Q2, Q3 ou Q4
# Ecriture d'un nouveau fichier csv a partir des 3 listes
def create_csv_scimago(csvsci, new_csv) :
    global cnt_scimago_jrn
    cnt_scimago_jrn+=1
    print ("begin create_csv_scimago")
    csv_sci_com = csv.reader(csvsci.splitlines(), delimiter=str(';'), quotechar='|')
    list_sci_com = []
    for row in list(csv_sci_com) :
        title = str(row[2])
        #print ("MY TITLE = ",title)
        title = title.replace('"','')
        title = correspondance_SJR_HAL(title)
        try :
            if (row[6] == "Q1") or (row[6] == "Q2") or (row[6] == "Q3") or (row[6] == "Q4") :
                list_sci_com.append((title,str(row[6])))
        except IndexError as inderr :
            print("Error for title ",title)
            

    with open(new_csv, 'w') as myfile:
        writefile = csv.writer(myfile, delimiter='|')#, quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for journal in list_sci_com :
            titre = journal[0]
            note = journal[1]
            try :
                writefile.writerow([titre, note])
            except Exception as e :
                print("Pb")



# Creation des fichiers CORE conferences contenant titre des conf, acronymes et notes
# Lecture du CSV telecharge envoye en parametre 
# Enregistrement nom_conf, acronyme et note dans 1 liste : list_core si note = A*, A, B ou C
# Ecriture d'un nouveau fichier csv a partir de la liste
def create_csv_core_conf(csv_core_conf, new_csv) :
    global cnt_core_conf
    cnt_core_conf+=1

    #csv_core = csv.reader(open(csv_core_conf,"rb"), delimiter=str(','), quotechar='"')
    csv_core = csv.reader(csv_core_conf.splitlines(), delimiter=str(','), quotechar='"')
    list_core = []
    global cnt_problems_sautligne
    for row in csv_core :
        title = str(row[1])
        if "\n" in title :
            title=title.replace("\r\n","") 
            cnt_problems_sautligne+=1
        acronym = str(row[2])
        note = str(row[4])
        if (note == "A*") or (note == "A") or (note == "B") or (note == "C") :
            list_core.append((title,acronym,note))

    with open(new_csv, 'w') as myfile:
        writefile = csv.writer(myfile, delimiter='|')
        for conf in list_core :
            titre = conf[0]
            acro = conf[1]
            note = conf[2]
            writefile.writerow([titre, acro, note])



class Command(BaseCommand):
    help = "renew all Scimago and Core CSV files from dictionaries in dict_scimago_core"

    def handle(self, *args, **options):

        # Download files from SCIMAGO and create CSV
        for scimago_filename in dict_scimago.keys() :
            url = dict_scimago[scimago_filename]
            file_sci_com = requests.get(url)
            create_csv_scimago(file_sci_com.content.decode("utf-8"), path_csv+scimago_filename)
            self.stdout.write(self.style.SUCCESS("create_csv_scimago {0}".format(scimago_filename)))
        
        for core_filename in dict_core.keys() :
            url = dict_core[core_filename]
            file_core_conf = requests.get(url)
            create_csv_core_conf(file_core_conf.content.decode("utf-8"),path_csv+core_filename)
            self.stdout.write(self.style.SUCCESS("create_csv_core {0}".format(core_filename)))

        print ("")
        print ("RESULTATS")
        print ("Fichiers CSV SCIMago : "+str(cnt_scimago_jrn))
        print ("Fichiers CSV CORE conf : "+str(cnt_core_conf))
        print ("problemes saut de lignes regles : "+str(cnt_problems_sautligne))
        print ("fin")

