from django.core.management.base import BaseCommand, CommandError
from haltools.settings  import BASE_DIR
import requests


class Command(BaseCommand):
    help = "Test sending file TEI to HAL through Sword API, just fill some variables to test"

    def handle(self, *args, **options):
        print("begin")
        errormsg = '' 
        requ_msg = ''
        login_user = 'bdoreau'
        id_hal_user = 'bastien-doreau'
        login_depot = 'bdoreau'
        passwd_depot = 'mbrv1232'
        
        namefile = BASE_DIR+"/xml_files/bdoreauCI1.xml"
        
        data = open(namefile)


        # HEADERS request 
        headers = {
            'Packaging': 'http://purl.org/net/sword-types/AOfr',
            'Content-Type': 'text/xml',
            'On-Behalf-Of': 'login|'+login_user+';idhal|'+id_hal_user,
        }

        try :
            response = requests.post('https://api-preprod.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot),timeout=60)
        except requests.exceptions.RequestException as e:
            print ("ERROR REQUEST {0}".format(e))
            requ_msg += "ERROR REQUEST : {0} -- ".format(e)
        except requests.exceptions.Timeout as t :
            print ("ERROR TIMEOUT REQUEST {0}".format(t))
            requ_msg += "ERROR TIMEOUT : {0} -- ".format(t)

        print("response POST : code {0}".format(response.status_code))
        print("response POST : text {0}".format(response.text))
        code_http = response.status_code
        if (code_http != 200) and (code_http != 202) :
            requ_msg += "RESPONSE : {0}".format(response.text)
        else :
            requ_msg += "RESPONSE : OK  {0}".format(errormsg)
        
        return None
