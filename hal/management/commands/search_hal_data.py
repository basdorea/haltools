####################################
# Script create CSV SCIMago
####################################
# SCIMAGO et CORE sont des agences de notation de journaux et conferences scientifiques
# SCIMAGO est generaliste et donne des notes Q1, Q2, Q3 et Q4 et ne note que les journaux
# https://www.scimagojr.com/
# CORE est specialise en informatique et donne des notes A*, A, B et C et note journaux et conferences
# http://portal.core.edu.au/conf-ranks/
#
# Ce script permet la creation de CSV contenant pour les journaux le titre et la note, et pour les conferences, le titre, l'acronyme et la note
# Principe : 
# - on telecharge des CSV a partir d'URL dans des fichiers
# - on parse les CSV en recuperant les infos souhaitees (titre et note ou titre, acronyme et note) dans une ou des listes
# - on reconstruit un CSV a partir de la ou les listes
#
# Pour SCIMAGO, les journaux sont recuperes par annee et 3 domaines sont analyses (donc 3 URL de telechargement par annee) Computer Science, Mats et Engineering
# Pour CORE, les conf sont recuperes par annee mais une seule base pour les journaux
####################################

from django.core.management.base import BaseCommand, CommandError
from haltools.settings  import BASE_DIR
from hal.dict_scimago_core import dict_scimago, dict_core
import requests
import csv
import os

###########
# VARIABLES
###########

path_csv = str(BASE_DIR+"/")
# Compteurs


class Command(BaseCommand):
    help = "renew all Scimago and Core CSV files from dictionaries in dict_scimago_core"

    def handle(self, *args, **options):
        csvf = "EC-isima-total.csv"
        listpub = []
        with open(csvf, 'r') as myfile:
            readfile = csv.reader(myfile, delimiter=',')

            for row in readfile :
                nom = row[0].capitalize()
                prenom = row[1].capitalize()
                idhal = row[2]

                if len(idhal) > 0 :
                    url = "https://api.archives-ouvertes.fr/search/?q=authIdHal_s:{0}&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s,journalTitle_s,conferenceTitle_s,citationFull_s&wt=json&fq=producedDateY_i:[2018%20TO%202023]".format(idhal)

                if len(idhal) == 0 :
                    url = 'https://api.archives-ouvertes.fr/search/?q=authLastNameFirstName_s:"{0}+{1}"&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s,journalTitle_s,conferenceTitle_s,citationFull_s&wt=json&fq=producedDateY_i:[2018%20TO%202023]'.format(nom,prenom)
                    #url = "https://api.archives-ouvertes.fr/search/?q=authFullName_s:{0} {1}&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s&wt=json&fq=producedDateY_i:[2018%20TO%202023]".format(nom,prenom)
                print(url)
                req = requests.get(url)

                self.stdout.write(self.style.SUCCESS("code HTTP {0}".format(req.status_code)))
                #print (req.status_code)
                #print (req.headers['content-type'])
                if (req.status_code == 200) :
                    try :
                        json = req.json()
                        # recup en json et on se place dans response puis docs
                        tous_docs = json['response']['docs']

                        # pour chaque element de 'docs', on recupere les elements de la publi que l'on place dans publilist
                        cptpub=0
                        for doc in tous_docs:
                            #print ("lecture reponse")
                            cptpub+=1


                            #recup halId
                            recup_halId = doc["halId_s"]
                            #halid = recup_halId#'\"{0}\"'.format(recup_halId)

                            # recup url
                            uri = doc["uri_s"]
                            #uri = '\"{0}\"'.format(uri)

                            #recup doctype
                            doctype = doc["docType_s"]
                            #doctype = '\"{0}\"'.format(doctype)

                            #recup title
                            title = doc["title_s"]
                            title[0].encode("utf-8")
                            titlefinal = title[0].replace("\""," ")

                            journal = None
                            conf = None
                            citation = None
                            try :
                                journal = doc["journalTitle_s"]
                            except Exception as e :
                                print("NO JOURNAL")
                            try :
                                conf = doc["conferenceTitle_s"]
                            except Exception as e :
                                print("NO CONF")
                            try :
                                citation = doc["citationFull_s"]
                                citations = citation.split('<a target=')
                                citation = citations[0]

                            except Exception as e :
                                print("N0 CITATIONS")

                            producedDate = doc["producedDate_s"]
                            if journal is not None :
                                jourconf = "Journal : {0}".format(journal)
                            elif conf is not None :
                                jourconf = "Conf : {0}".format(conf)
                            else :
                                jourconf = ""

                            print("{0} {1} -> {2} {3} {4} {5} {6}".format(nom, prenom, titlefinal, recup_halId, uri,doctype, producedDate))
                            listpub.append((nom, prenom, titlefinal, recup_halId, uri,doctype, producedDate, jourconf, citation ))
                    except Exception as e :
                        print("Problem JSON pour {0} {1}".format(nom,prenom))
                else :
                    print("Problem requete pour {0} {1}".format(nom,prenom))

        with open("resultat_searchhaldata.csv", 'w') as myfile:
            writefile = csv.writer(myfile, delimiter=',')

            for pub in listpub :
                nom = pub[0]
                prenom = pub[1]
                titre = pub[2]
                halid = pub[3]
                uri = pub[4]
                doctype = pub[5]
                datepub = pub[6]
                jourconf = pub[7]
                citation = pub[8]
                writefile.writerow([nom, prenom, titre, halid, uri, doctype, datepub, jourconf, citation])


        print ("")
        print ("FIN")


