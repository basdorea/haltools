import requests
from celery import shared_task

from utils import mails

@shared_task
def searchdatahal(mailretour,  annee_debut, annee_fin, users_hal):
    reponse_mail = "Bonjour\n\nVous avez demandé la liste des publications pour les idHal suivants {0}\n\n".format(users_hal)
    reponse_mail += "Les informations présentées sont de la forme : idHal utilisateur ; Titre ; halId publi ; URL ; Type ; Date \n"
    reponse_mail += "Vous pouvez récupérer les données et les importer comme un CSV avec ';' comme séparateur"
    reponse_mail += "Voici le résultat de la recherche dans HAL :\n\n"


    list_users = users_hal.split(',')
    for user in list_users :
        idhal = user.strip()
        #reponse_mail += "idHal : {0}\n".format(idhal)

        if len(idhal) > 0 :
            url = "https://api.archives-ouvertes.fr/search/?q=authIdHal_s:{0}&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s,journalTitle_s,conferenceTitle_s,citationFull_s&wt=json&fq=producedDateY_i:[{1}%20TO%20{2}]".format(idhal, annee_debut, annee_fin)

            #url = 'https://api.archives-ouvertes.fr/search/?q=authLastNameFirstName_s:"{0}+{1}"&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s&wt=json&fq=producedDateY_i:[2018%20TO%202023]'.format(nom,prenom)
            #url = "https://api.archives-ouvertes.fr/search/?q=authFullName_s:{0} {1}&start=0&rows=200&fl=uri_s,halId_s,docType_s,producedDate_s,title_s&wt=json&fq=producedDateY_i:[2018%20TO%202023]".format(nom,prenom)
        print(url)
        req = requests.get(url)

        print("code HTTP {0}".format(req.status_code))
        #print (req.status_code)
        #print (req.headers['content-type'])
        if (req.status_code == 200) :
            #toto = True
            #if toto :
            try :
                json = req.json()
                # recup en json et on se place dans response puis docs
                tous_docs = json['response']['docs']

                # pour chaque element de 'docs', on recupere les elements de la publi que l'on place dans publilist
                cptpub=0
                for doc in tous_docs:
                    #print ("lecture reponse")
                    cptpub+=1


                    #recup halId
                    recup_halId = doc["halId_s"]
                    #halid = recup_halId#'\"{0}\"'.format(recup_halId)

                    # recup url
                    uri = doc["uri_s"]
                    #uri = '\"{0}\"'.format(uri)

                    #recup doctype
                    doctype = doc["docType_s"]
                    #doctype = '\"{0}\"'.format(doctype)

                    #recup title
                    title = doc["title_s"]
                    title[0].encode("utf-8")
                    titlefinal = title[0].replace("\""," ")

                    journal = None
                    conf = None
                    citation = None
                    try :
                        journal = doc["journalTitle_s"]
                    except Exception as e :
                        print("NO JOURNAL")
                    try :
                        conf = doc["conferenceTitle_s"]
                    except Exception as e :
                        print("NO CONF")
                    try :
                        citation = doc["citationFull_s"]
                        citations = citation.split('<a target=')
                        citation = citations[0]

                    except Exception as e :
                        print("N0 CITATIONS")


                    producedDate = doc["producedDate_s"]
                    if journal is not None :
                        jourconf = "Journal : {0}".format(journal)
                    elif conf is not None :
                        jourconf = "Conf : {0}".format(conf)
                    else :
                        jourconf = ""

                    print("{0} -> {1} {2} {3} {4} {5} - journal {6}, conf {7}, citations {8}".format(idhal, titlefinal, recup_halId, uri, doctype, producedDate, journal, conf, citation))
                    reponse_mail += "{0} ; {1} ; {2} ; {3} ; {4} ; {5}; {6} ; Citation : {7}\n".format(idhal, titlefinal, recup_halId, uri, doctype, producedDate, jourconf, citation)
            except Exception as e :
                print("Problem JSON pour {0}".format(idhal))
                reponse_mail+="Problème au niveau du format JSON de sortie\n" 
        else :
            print("Problem requete pour {0} {1}".format(idhal))
            reponse_mail+="Problème sur la requête HTTP\n"
        
        #reponse_mail +="\n"

    mails.sendonemail("[HALTOOLS] résultats recherche",reponse_mail,mailretour)
    return None