# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
#from django.utils.encoding import python_2_unicode_compatible

from django.contrib.auth.models import User, Group
from datetime import datetime

#@python_2_unicode_compatible
class Referent(models.Model):

    referent = models.OneToOneField(User, on_delete=models.CASCADE, default=None, blank=True, null=True)  
    login_hal = models.CharField(max_length=20, default='login_hal', verbose_name=_("login_hal"))
    pwd_hal = models.CharField(max_length=20, default='pwd_hal', verbose_name=_("pwd_hal"))
    def __str__(self):
        return self.referent
        
