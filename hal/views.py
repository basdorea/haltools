# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect

from django_auth_ldap.backend import LDAPBackend
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

#from django.contrib.auth.views import logout
from django.contrib import messages
from django.core.checks.messages import Error
from django.db.models import Q
from django.contrib.auth.models import User, Permission

from .forms import ConnexionForm, Bibtex2halForm, BibtexXmlForm, Csv2halForm, VerifHalConfForm, Bibformat2halForm ,SearchDataHalForm, \
    TextXmlForm, ModifTextForm,TextFromRG, Text2halForm

from haltools.settings import MEDIA_ROOT

from .scripts_bibtex import script_bibtex_2_hal
#from .scripts_text import script_text_2_hal
from .scripts_csv import script_csv_2_hal
from .scriptupdatehalranking import script_verify_hal_ranking #, script_verif_hal_ranking
from  .create_xml2hal import sendselect_2hal
from .searchdatahal import searchdatahal

from slugify import slugify
from datetime import datetime

import random
import string
#import httplib
#import xml.dom.minidom
import csv
import re

##################################################################################################################################
## SIMPLE VIEWS  #################################################################################################################
##################################################################################################################################

# return index
def index(request):
    print("test")
    return render(request, 'hal/index.html')
    
# allows connection through table User in database haltools
def connexion_old(request):
    reponse = ""
    form = ConnexionForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        #print ('username :', username)
        authmodel = ModelBackend() #authentification backend django
        
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
        else:
            reponse = ('Erreur d authentification')
                    
    return render(request, 'hal/connexion.html', locals())

# allows connection through table User in database haltools
def connexion(request):
    reponse = ""
    form = ConnexionForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        try :
            authldap = LDAPBackend() #authentification par le LDAP defini dans settings.py
            authmodel = ModelBackend()
            userldap = authldap.authenticate(request=request, username=username, password=password)
        
            if userldap is not None:
                usermodels = User.objects.filter(Q(username=userldap.username) & Q(last_name=userldap.last_name) & Q(first_name=userldap.first_name))
                if len(usermodels) == 0 :
                    # jamais connecte, creation ds Django Auth User
                    print("create Auth User for {0}".format(userldap.username))
                    uniq_user = User.objects.create(username=userldap.username,last_name=userldap.last_name,first_name=userldap.first_name, email=userldap.mail)
                    login (request, uniq_user, backend='django_auth_ldap.backend.LDAPBackend') 
                elif len(usermodels) == 1 :
                    # OK, connexion
                    uniq_user = usermodels[0]
                    login (request, uniq_user, backend='django_auth_ldap.backend.LDAPBackend') 
                else :
                    # probleme plusieurs users pr le meme username
                    print("PROBLEM")
                    reponse = 'Erreur d authentification'

        except authldap.ldap.INVALID_CREDENTIALS:
            authldap.ldap_client.unbind()
            print ('Wrong username password')
            reponse = 'Erreur d authentification'
                    
    return render(request, 'hal/connexion.html', locals())
    
# disconnect user
def deconnexion(request):
    logout(request)
    return render(request, 'hal/connexion.html', locals())

# return blog
def article_blog(request):
    return render(request, 'hal/blog.html', locals())

# return aide
def aide(request):
    return render(request, 'hal/aide.html', locals())

# Class used to describe a publication
class PubliDescribe():
    """
        Class used to re-create a list of all the publi 
    """
    def __init__(self):
        self.num = ""
        self.authors =""
        self.title = ""
        self.conf = ""
        self.page = ""
        self.vol = ""
        self.date = ""
        self.type = ""
        self.todo = ""
        self.ville = "" 
        self.pays = ""
        self.acr = ""
        self.doi = ""
        self.editor = ""
        self.lang = ""


class PubliChange():
    """
        Class used to re-create a list of all the publi 
    """
    def __init__(self):
        self.type = ""
        self.title = ""
        self.uri = ""
        self.date = ""
        self.change = ""
        self.conf = ""
        self.conf2 = ""
        self.note = ""
        self.owner = ""

##################################################################################################################################
## BIBTEX2HAL  ######################################################################################################################
##################################################################################################################################

@login_required
def bibtex2hal(request):
    '''
        treats a bibtex file and some variables
        give a table of data (parsed bibtex file) and results of HAL depot
    '''   
    print("method views.bibtex2hal")
    reponse = ""
    csvfile = ""
    form = Bibtex2halForm(request.POST or None)
    
    if form.is_valid():
        name_user = form.cleaned_data['name_user']
        firstname_user = form.cleaned_data['firstname_user']
        labo_auth_final = form.cleaned_data['labo_auth_final']
        bibtex_file = form.cleaned_data['bibtex_file']       

        labo_auth_final = "struct-"+labo_auth_final


        print ("begin script")

        today = datetime.today()
        randomnumbers = ''.join(random.choices(string.digits, k=6))
        csvfile = "{0}_{1}-{2}_{3}".format(slugify(name_user),today.day, today.month,randomnumbers)

        msg_to_announce = "Attendre quelques minutes avant d'aller voir le résultat"
        reponse = script_bibtex_2_hal.delay(bibtex_file, name_user, firstname_user, labo_auth_final, csvfile)
        messages.add_message(request, messages.WARNING, msg_to_announce)
        
    return render(request, 'hal/bibtex2hal.html', locals())


@login_required
def bibformat_2hal(request, csvfile):
    print("method views.bibformat_2hal")
    form = Bibformat2halForm(request.POST or None)
    try :

        with open('bibtex_csv_files/'+csvfile+'.csv', 'r', newline='') as csv_file :
            csvr = csv.reader(csv_file, delimiter =',')

            list_to_post = []
            for row in csvr :
                print(row)
                p = PubliDescribe() #PubliChange()

                p.num = row[0]
                p.authors =row[1]
                p.title = row[2]
                p.conf = row[3]
                p.page = row[4]
                p.vol = row[5]
                p.date = row[6]
                p.type = row[7]
                p.todo = row[8]
                p.ville = row[9] 
                p.pays = row[10]
                p.acr = row[11]
                p.doi = row[12]
                p.editor = row[13]
                p.lang = row[14]

                list_to_post.append(p)

        with open('bibtex_csv_files/result_'+csvfile+'.txt', 'r') as resultxt :
            result = resultxt.readlines()

        if form.is_valid():
            print("method views.bibformat_2hal FORM VALID")
            name_user = form.cleaned_data['name_user']
            firstname_user = form.cleaned_data['firstname_user']
            login_user = form.cleaned_data['login_user']
            id_hal_user = form.cleaned_data['id_hal_user']
            mail_reponse = form.cleaned_data['mail_reponse']
            labo_auth_final = form.cleaned_data['labo_auth_final']
            domains = form.cleaned_data['domains']
            domains2 = form.cleaned_data['domains2']
            login_depot = form.cleaned_data['login_depot']
            passwd_depot = form.cleaned_data['passwd_depot']
            choice_depot = form.cleaned_data['choice_depot']

            list_publis_to_update = request.POST.getlist('chkbox')

            labo_auth_final = "struct-"+labo_auth_final
            listdomains = domains + domains2  
            for pub_upd in list_publis_to_update :
                print(pub_upd)
            
            msg_to_announce = "Après l'opération, les résultats seront envoyés à l'adresse : {0}".format(mail_reponse)
            messages.add_message(request, messages.WARNING, msg_to_announce)

            reponse = sendselect_2hal.delay(firstname_user,name_user,login_user,id_hal_user,mail_reponse,labo_auth_final,listdomains,login_depot,passwd_depot,choice_depot,list_publis_to_update,csvfile)
            

        return  render(request, 'hal/bibformat_2hal.html', locals())
    except Exception as e :
        print (e)
        return redirect('https://http.cat/425')


## A enlever
""" def validatepost2hal(request):
    print("method views.validatepost2hal")

    if request.method == 'POST' :
        print("method post")
        list_publis_to_update = request.POST.getlist('chkbox')
        #print(list_publis_to_update)
        for pub_upd in list_publis_to_update :
            print(pub_upd)

    return redirect('https://http.cat/425') """


##################################################################################################################################
## UPDATE_HAL  ######################################################################################################################
##################################################################################################################################

@login_required
def verifhalconf_ranking(request):
    '''
        update HAL from Bitex DBLP -> change name conf
    '''   
    print("method views.verifhalconf_ranking")
    reponse = ""
    url_result = ""
    form = VerifHalConfForm(request.POST or None)

    csvfile = ""
    
    if form.is_valid():
        # get data from form

        name_user = form.cleaned_data['name_user']
        firstname_user = form.cleaned_data['firstname_user']
        id_hal_user = form.cleaned_data['id_hal_user']
        
        print ("begin script")

        today = datetime.today()
        randomnumbers = ''.join(random.choices(string.digits, k=6))
        csvfile = "{0}_{1}-{2}_{3}".format(slugify(name_user),today.day, today.month,randomnumbers)

        msg_to_announce = "Attendre quelques minutes avant d'aller voir le résultat"
        messages.add_message(request, messages.WARNING, msg_to_announce)
        reponse = script_verify_hal_ranking.delay(name_user, firstname_user, id_hal_user, csvfile)        
        
    return render(request, 'hal/verifhalconf_ranking.html', locals())

@login_required
def verifhal_byuser(request, csvfile):
    print("method views.verifhal_byuser")
    try :

        with open('ranking_csv_files/'+csvfile+'.csv', 'r', newline='') as csv_file :
            csvr = csv.reader(csv_file, delimiter =',')

            list_to_post = []
            for row in csvr :
                print(row)
                p = PubliChange()
                p.type = row[0]
                p.title = row[1]
                p.uri = row[2]
                p.date = row[3]
                p.change = row[4]
                p.conf = row[5]
                p.conf2 = row[6]
                p.note = row[7]
                p.owner = row[8]
                
                list_to_post.append(p)

        return  render(request, 'hal/verifhal_byuser.html', locals())
    except Exception as e :
        print (e)
        return redirect('https://http.cat/425')


@login_required
def valid_post_hal(request):
    print("method views.valid_update_hal")
    print(request)
    if request.method == 'POST' :
        print("method post")
        list_publis_to_update = request.POST.getlist('chkbox')
        #print(list_publis_to_update)
        for pub_upd in list_publis_to_update :
            print(pub_upd)
        
        #reponse = script_update_hal_ranking.delay(list_publis_to_update)
        
    return render(request,'hal/updatehal_byuser.html', locals() )


##################################################################################################################################
## SEARCHHALDATA  ################################################################################################################
##################################################################################################################################

@login_required
def search_hal_data(request):

    reponse = ""
    csvfile = ""
    form = SearchDataHalForm(request.POST or None)

    if form.is_valid():
        mail_response = form.cleaned_data['mail_response']
        annee_debut = form.cleaned_data['annee_debut']
        annee_fin = form.cleaned_data['annee_fin']
        users_hal = form.cleaned_data['users_hal']

        if int(annee_debut) >= int(annee_fin) :
            msg_to_announce = "Problème : l'année de début doit être inférieure à l'année de fin {0}".format(mail_response)

        else :
            searchdatahal.delay(mail_response, annee_debut, annee_fin, users_hal)

            msg_to_announce = "Un mail vous sera envoyé à l'adresse suivante : {0}".format(mail_response)
        
        messages.add_message(request, messages.WARNING, msg_to_announce)

    return render(request, 'hal/searchdatahal.html', locals())

##################################################################################################################################
## CSV2HAL  #####################################################################################################################
##################################################################################################################################

## NOT USED
@login_required
def csv2hal(request):
    reponse = ""
    csvfile = ""
    form = Csv2halForm(request.POST or None)
    
    if form.is_valid():
        name_user = form.cleaned_data['name_user']
        firstname_user = form.cleaned_data['firstname_user']
        labo_auth_final = form.cleaned_data['labo_auth_final']
        csv_source = form.cleaned_data['csv_file']       

        labo_auth_final = "struct-"+labo_auth_final

        print ("begin script csv2hal")

        today = datetime.today()
        randomnumbers = ''.join(random.choices(string.digits, k=6))
        csvfile = "{0}_{1}-{2}_{3}".format(slugify(name_user),today.day, today.month,randomnumbers)

        msg_to_announce = "Attendre quelques minutes avant d'aller voir le résultat"
        reponse = script_csv_2_hal.delay( name_user, firstname_user, labo_auth_final, csv_source, csvfile)
        messages.add_message(request, messages.WARNING, msg_to_announce)

    """ reponse = ""
    form = Csv2halForm(request.POST or None)
    
    if form.is_valid():
        # recup donnees
        form_author = form.cleaned_data['form_author']
        name_user = form.cleaned_data['name_user']
        firstname_user = form.cleaned_data['firstname_user']
        labo_auth_final = form.cleaned_data['labo_auth_final']
        id_hal_user = form.cleaned_data['id_hal_user']
        login_user = form.cleaned_data['login_user']
        domain1 = form.cleaned_data['domain1']
        domain2 = form.cleaned_data['domain2']
        domain3 = form.cleaned_data['domain3']
        domain4 = form.cleaned_data['domain4']
        domain5 = form.cleaned_data['domain5']

        choice_source = form.cleaned_data['choice_source']
    
        login_depot = form.cleaned_data['login_depot']
        passwd_depot = form.cleaned_data['passwd_depot']
        #bool_depot_preprod = form.cleaned_data['bool_depot_preprod']
        #bool_depot_prod = form.cleaned_data['bool_depot_prod']
        choice_depot = form.cleaned_data['choice_depot']
        csv_file = form.cleaned_data['csv_file']

        bool_depot_preprod = False
        bool_depot_prod = False
        
        # recup du type de depot
        if choice_depot == 'PREPROD' :
            print ("Views.py depot preprod")
            bool_depot_preprod = True
        if choice_depot == 'PRODHAL' :
            print ("Views.py depot prod")
            bool_depot_prod = True

        
        labo_auth_final = "struct-"+labo_auth_final

        #gestion domaines  domain1 obligatoire
        listdomains = []
        listdomains.append(domain1)
        if domain2 is not "" :
            listdomains.append(domain2)
        if domain3 is not "" :
            listdomains.append(domain3)
        if domain4 is not "" :
            listdomains.append(domain4)
        if domain5 is not "" :
            listdomains.append(domain5)

        single = False

        #print ("begin script")
        print ("form_author "+form_author)
        reponse = script_csv_2_hal (csv_file, form_author, name_user, firstname_user, labo_auth_final, id_hal_user, login_user, listdomains, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod, single, choice_source)

        reponse_to_post = ""
        list_to_post = []
        for result in reponse :
            if result[0] == "RESULTATS" :
                reponse_to_post = reponse_to_post+"<br/><br/><b>"+result[1]+"</b><br/>"+result[2]
            else :
                p = PubliDescribe()
                p.num = result[0]
                p.authors = result[1]
                p.title = result[2]
                p.conf = result[3]
                p.page = result[4]
                p.vol = result[5]
                p.date = result[6]
                p.type = result[7]
                p.todo = result[8]
                p.ville = result[9]
                p.pays = result[10]
                p.acr = result[11]
                p.lang = result[12]
                
                list_to_post.append(p)
                #list_to_post.append((result[0],result[1],result[2],result[3],result[4],result[5],\
                #result[6],result[7],result[8],result[9],result[10],result[11],result[12],))
        #reponse=("reponse HAL") """
    return render(request, 'hal/csv2hal.html', locals())





