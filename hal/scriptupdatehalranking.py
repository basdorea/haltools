
import requests
from celery import shared_task
import difflib 
import csv
import re

from haltools.settings import BASE_DIR
from .dict_scimago_core import dict_scimago, dict_core
from .dict_countries import dict_countries

import bibtexparser
from bibtexparser.bparser import BibTexParser
#from bibtexparser.customization import homogenize_latex_encoding
from bibtexparser.customization import convert_to_unicode

def get_conf_acronym(conf):
    print("TRY ACRONYM for {0}".format(conf))
    acronym = None
    if ('(' in conf) and (')' in conf) :
        try : 
            firstparent = conf.index('(')
            secondparent = conf.index(')')
            string = conf[firstparent+1:secondparent]
            acronym = re.sub('\d+', '', string)
            acronym = acronym.strip()
            print("TEST ACRONYM  : Conf {0} , string {1} , acronym {2}".format(conf, string, acronym))
        except Exception as ex :
            print("ERROR get_conf_acronym {0}".format(ex))
    return acronym


@shared_task
def script_verify_hal_ranking(name_user, firstname_user, id_hal_user, name_file):
    # les listes suivantes correspondent aux annees pour lesquelles on a 
    #list_years_scimago =[2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026]
    # list_years_core =[2013,2014,2017,2018,2020,2021,2022,2023,2024,2025,2026]

    fullname1 = "{0} {1}".format(firstname_user.lower(), name_user.lower())
    fullname2 = "{0} {1}".format(name_user.lower(), firstname_user.lower())

    name_csv_tmpfile = "{0}_tmp.csv".format(name_file)
    name_csv_file = "{0}.csv".format(name_file)

    # recup de toutes les publis par HalID
    url_request = "https://api.archives-ouvertes.fr/search/?q=authIdHal_s:{0}&start=0&rows=1000&fl=uri_s,docType_s,halId_s,authFullName_s,authIdHal_s,title_s,journalTitle_s,conferenceTitle_s,producedDate_s,contributorFullName_s,contributorId_i,owners_i&wt=json".format(id_hal_user)

    req = requests.get(url_request)       
    json = ""
    try :
        json = req.json()
    except ValueError as ve :
        print ("PROBLEME VALUEERROR {0}".format(ve))
    try : 
        if json is not "" :
            all_publis = json['response']['docs']

            foundcontrib_id = False
            contrib_id = ""

            # open the file in the write mode
            with open('ranking_csv_files/'+name_csv_tmpfile, 'w') as csvf:
                # create the csv writer
                csvwriter = csv.writer(csvf, delimiter=',')
                cnt_publis = 0
                for pub in all_publis :
                    cnt_publis+=1
                    print("------------ publi {0}".format(cnt_publis))

                    uri = pub["uri_s"]
                    title = pub["title_s"]
                    datep = pub["producedDate_s"]
                    datey = datep.split('-')
                    datey = int(datey[0])
                    journ = ""
                    conf = ""
                    note = ""
                    contributor_name = pub["contributorFullName_s"]
                    contributor_id = str(pub["contributorId_i"])
                    owners = str(pub["owners_i"])
                    allowners =  owners# ','.join(owners)
                    
                    if foundcontrib_id == False :
                        try :
                            if (contributor_name.lower() == fullname1) or (contributor_name.lower() == fullname2) :
                                print("find contrib_id = {0}".format(contrib_id))
                                contrib_id = contributor_id
                                foundcontrib_id = True
                        except Exception as e :
                            pass

                    row=[]
                    ## on veut une reponse du type :
                    ## ART|"nom de ma publi"|DIFF|journal_publi|journalscimago|proprietaire(o/n)
                    print("publi num {0} au titre {1}".format(cnt_publis, title))

                    if pub["docType_s"] == "ART" :
                        try :
                            journ = pub["journalTitle_s"]
                        except Exception as e:
                            pass
                        print("ARTICLE : {0} -> {1} -- {2} -- {3}".format(title, uri, journ, datey))
                        
                        art_file = None

                        list_scimago_files = dict_scimago.keys()
                        for file in list_scimago_files :
                            year = "20"+file[-6:-4]
                            #print("year {0} of file : {1} and date_pub={2}".format(year,file,datey))
                            if datey >= int(year) :
                                #print("elif match date_pub={0} year ={1}".format(datey,year))
                                art_file=file
                                break
                            else :
                                continue
                        print('artfile after loop {0}'.format(art_file))

                        """ if datey <= 2013 :
                            art_file = 'SCIMago-J-2013.csv'
                        elif datey == 2014 :
                            art_file = 'SCIMago-J-2014.csv'
                        elif datey == 2015 :
                            art_file = 'SCIMago-J-2015.csv'
                        elif datey == 2016 :
                            art_file = 'SCIMago-J-2016.csv'
                        elif datey == 2017 :
                            art_file = 'SCIMago-J-2017.csv'
                        elif datey == 2018 :
                            art_file = 'SCIMago-J-2018.csv'
                        elif datey == 2019 :
                            art_file = 'SCIMago-J-2019.csv'
                        elif datey == 2020 :
                            art_file = 'SCIMago-J-2020.csv'
                        elif datey >= 2021 :
                            art_file = 'SCIMago-J-2021.csv'
                        else :
                            print ("Problem in date {0}".format(datey)) """

                        if art_file != None :
                            list_journals = []
                            csvscimago = open('ranking/{0}'.format(art_file),"r")
                            csvscimagoreader = csv.reader(csvscimago, delimiter=str('|'))
                            for row in csvscimagoreader :
                                list_journals.append(row[0])

                            newjourn = ""
                            note = ""
                            try :
                                result = difflib.get_close_matches(journ,list_journals)
                                print("For journal '{0}'  results are '{1}'".format(journ, result))
                                if journ != result[0] :
                                    newjourn = result[0]
                                    print("DIFFERENCE between {0} and {1} --> UPDATE".format(journ,result[0]))
                                    #reponse.append("ART|{0}|{1}|DIFF|{2}|{3}|{4}".format(title,datey, journ, result[0],allowners))
                                    res = "DIFF"
                                    ## find note
                                    print(f"find note art search {result[0]}")
                                    csvscimago.seek(0)
                                    for row in csvscimagoreader :
                                        if result[0] == row[0] :
                                            note = row[1]

                                    #row = ["ART",title,datey,"DIFF",journ,result[0],allowners]
                                else :
                                    #reponse.append("ART|{0}|{1}|CORRECT|{2}||{3}".format(title,datey,journ, allowners))
                                    res = "CORRECT"
                                    ## find note
                                    print(f"find note art search {result[0]}")
                                    csvscimago.seek(0)
                                    for row in csvscimagoreader :
                                        if result[0] == row[0] :
                                            note = row[1]
                                    #row = ["ART",title,datey,"DIFF",journ,result[0],allowners]
                            except Exception as e :
                                print("Journal {0} NOT FOUND in list".format(journ))
                                #reponse.append("ART|{0}|{1}|NONE|{2}||{3}".format(title,datey, journ, allowners))
                                res = "NONE"

                            print("")
                            row = ["ART",title,uri,datey,res,journ,newjourn,note,allowners]
                        else :
                            print("Pas de correspondance de fichier")
                            res = "TOO OLD"
                            row = ["ART",title,uri,datey,res,journ,'',note,allowners]

                    elif pub["docType_s"] == "COMM" :
                        try :
                            conf = pub["conferenceTitle_s"]
                        except Exception as e:
                            pass
                        print("CONF : {0} -> {1} -- {2} -- {3}".format(title, uri, conf, datey))


                        conf_file = None

                        list_core_files = dict_core.keys()
                        for file in list_core_files :
                            year = "20"+file[-6:-4]
                            #print("year {0} of file : {1} and datey={2}".format(year,file,datey))
                            if datey >= int(year) :
                                #print("elif match datey={0} year ={1}".format(datey,year))
                                conf_file=file
                                break
                            else :
                                continue
                        print('conf_file after loop {0}'.format(conf_file))

                        """ if datey <= 2013 :
                            conf_file = 'CORE-C-2013.csv'
                        elif datey == 2014 or datey == 2015 or datey == 2016:
                            conf_file = 'CORE-C-2014.csv'
                        elif datey == 2017 :
                            conf_file = 'CORE-C-2017.csv'
                        elif datey == 2018 or datey == 2019 :
                            conf_file = 'CORE-C-2018.csv'
                        elif datey == 2020 :
                            conf_file = 'CORE-C-2020.csv'
                        elif datey >= 2021 :
                            conf_file = 'CORE-C-2021.csv'
                        else :
                            print ("Problem in date {0}".format(datey)) """

                        if conf_file != None :
                            list_confs = []
                            list_confs_acr = []
                            csvcore = open('ranking/{0}'.format(conf_file),"r")
                            csvcorereader = csv.reader(csvcore, delimiter=str('|'))
                            for row in csvcorereader :
                                list_confs.append(row[0])
                                list_confs_acr.append(row[1])
                            
                            newconf = ""
                            note =""


                            try :
                                matchconf = False
                                if conf in list_confs :
                                    res = "CORRECT"
                                    matchconf = True
                                    ## find note
                                    print("find note comm")
                                    csvcore.seek(0)
                                    for row in csvcorereader :
                                        if conf == row[0] :
                                            note = row[2]

                                else :
                                    acro_conf = get_conf_acronym(conf)
                                    if acro_conf :
                                        if acro_conf in list_confs_acr :
                                            res = "CORRECT ACR"
                                            matchconf = True
                                            ## find note
                                            print("find note comm")
                                            csvcore.seek(0)
                                            for row in csvcorereader :
                                                if acro_conf == row[1] :
                                                    note = row[2]
                                    if not matchconf :
                                        result = difflib.get_close_matches(conf,list_confs)
                                        if conf == result[0] :
                                            res = "CORRECT BIZZARE"
                                            matchconf = True
                                            ## find note
                                            print("find note comm")
                                            csvcore.seek(0)
                                            for row in csvcorereader :
                                                if result[0] == row[0] :
                                                    note = row[2]
                                        if conf != result[0] :
                                            newconf = result[0]
                                            print("DIFFERENCE between {0} and {1} --> UPDATE".format(conf,result[0]))
                                            res = "DIFF"
                                            matchconf = True
                                            ## find note
                                            csvcore.seek(0)
                                            for row in csvcorereader :
                                                if result[0] == row[0] :
                                                    note = row[2]

                            
                            except Exception as e :
                                print("Conf {0} NOT FOUND in list - ERROR {1}".format(conf, e))
                                #reponse.append("COMM|{0}|{1}|NONE|{2}||{3}".format(title,datey,conf, allowners))
                                res = "NONE"

                            """ try :
                                result = difflib.get_close_matches(conf,list_confs)                                    
                                if not result : # empty list
                                    result = difflib.get_close_matches(conf,list_confs_acr)
                                    if not result : # empty list
                                        # on essaie d'isoler l'acronyme de la conf
                                        acronym_conf = get_conf_acronym(conf)
                                        result = difflib.get_close_matches(acronym_conf,list_confs_acr)

                                print("For conf '{0}'  results are '{1}'".format(conf, result))

                                if conf != result[0] :

                                    newconf = result[0]
                                    print("DIFFERENCE between {0} and {1} --> UPDATE".format(conf,result[0]))
                                    #reponse.append("COMM|{0}|{1}|DIFF|{2}|{3}|{4}".format(title,datey,conf, result[0],allowners))
                                    res = "DIFF"
                                else :
                                    #reponse.append("COMM|{0}|{1}|CORRECT|{2}||{3}".format(title,datey,conf, allowners))
                                    res = "CORRECT"
                            except Exception as e :
                                print("Conf {0} NOT FOUND in list".format(conf))
                                #reponse.append("COMM|{0}|{1}|NONE|{2}||{3}".format(title,datey,conf, allowners))
                                res = "NONE" """
                            print("")
                            row = ["COMM",title,uri,datey,res,conf,newconf,note,allowners]
                        else :
                            print("Pas de correspondance de fichier")
                            res = "TOO OLD"
                            row = ["COMM",title,uri,datey,res,conf,'',note,allowners]
                    
                    else : #other types of publis
                        row = [pub["docType_s"],title,'','','','','','','']
                    
                    csvwriter.writerow(row)

            #replace in reponse allowners by Y or N if user  can change publi (owner)
            with open('ranking_csv_files/'+name_csv_tmpfile, 'r', newline='') as source, open('ranking_csv_files/'+name_csv_file, 'w', newline='') as result:
                csvreader = csv.reader(source)
                csvwriter = csv.writer(result)

                # Process data rows
                for row in csvreader:
                    
                    if contrib_id != "":
                        if contrib_id in row[8] :
                            row[8] = "Y"
                        else :
                            row[8] = "N"
                    else :
                        row[8] = "N"
                    
                    csvwriter.writerow(row)    

    except KeyError as ke :
        print(ke)
    return None
