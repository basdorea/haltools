#-*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

from django.utils.translation import ugettext as _
from django.contrib.admin import widgets


class ConnexionForm(forms.Form):
    username = forms.CharField(required=True, max_length=40, label="Login")
    password = forms.CharField(required=True, max_length=400, label=("Mot de passe"), widget=forms.PasswordInput())


CHOICES_DEPOT= (
#('NODEPOT', 'Pas de dépôt'),
('PREPROD', 'Dépôt préprod'),
('PRODHAL', 'Dépôt HAL'),
)

CHOICES_SOURCE_BIBTEX = (
('DBLP','DBLP'),
('ENDNOTE','ENDNOTE'),    
)

CHOICES_SOURCE_CSV = (
('PUBMED','PUBMED'),     
#('SCOPUS','SCOPUS'), 
)

CHOICES_SOURCE_TEXT = (
('RG','Research Gate'),     
)



CHOICES_DOMAINS = (
("math","Maths"),
("spi","Sciences de l'ingenieur"),
("sdv","Sciences du vivant"),
("sde","Sciences de l'environnement"),
("phys","Physique"),
("shs","Sciences de l'homme et de la societe"),
("sdu","Planetes et univers"),
("stat","Statistiques"),
("info.eiah","informatique Environnementsinformatiques pour l'Apprentissage Humain"),
("info.info-ai","informatique/Intelligence artificielle"),
("info.info-ao","informatique/Arithmétique des ordinateurs"),
("info.info-ar","informatique/Architectures Matérielles"),
("info.info-au","informatique/Automatique"),
("info.info-bi","informatique/Bio-informatique"),
("info.info-bt","informatique/Biotechnologie"),
("info.info-cc","informatique/Complexité"),
("info.info-ce","informatique/Ingénierie, finance et science"),
("info.info-cg","informatique/Géométrie algorithmique"),
("info.info-cl","informatique/Informatique et langage"),
("info.info-cr","informatique/Cryptographie et sécurité"),
("info.info-cv","informatique/Vision par ordinateur et reconnaissance de formes"),
("info.info-cy","informatique/Ordinateur et société"),
("info.info-db","informatique/Base de données"),
("info.info-dc","informatique/Calcul parallèle, distribué et partagé"),
("info.info-dl","informatique/Bibliothèque électronique"),
("info.info-dm","informatique/Mathématique discrète"),
("info.info-ds","informatique/Algorithme et structure de données"),
("info.info-es","informatique/Systèmes embarqués"),
("info.info-et","informatique/Technologies Émergeantes"),
("info.info-fl","informatique/Théorie et langage formel"),
("info.info-gl","informatique/Littérature générale"),
("info.info-gr","informatique/Synthèse d'image et réalité virtuelle"),
)

CHOICES_DOMAINS2 = (
("info.info-gt","informatique/Informatique et théorie des jeux"),
("info.info-hc","informatique/Interface homme-machine"),
("info.info-ia","informatique/Ingénierie assistée par ordinateur"),
("info.info-im","informatique/Imagerie médicale"),
("info.info-ir","informatique/Recherche d'information"),
("info.info-it","informatique/Théorie de l'information"),
("info.info-iu","informatique/Informatique ubiquitaire"),
("info.info-lg","informatique/Apprentissage"),
("info.info-lo","informatique/Logique eninformatique"),
("info.info-ma","informatique/Système multi-agents"),
("info.info-mc","informatique/Informatique mobile"),
("info.info-mm","informatique/Multimédia"),
("info.info-mo","informatique/Modélisation et simulation"),
("info.info-ms","informatique/Logiciel mathématique"),
("info.info-na","informatique/Analyse numérique"),
("info.info-ne","informatique/Réseau de neurones"),
("info.info-ni","informatique/Réseaux et télécommunications"),
("info.info-oh","informatique/Autre"),
("info.info-os","informatique/Système d'exploitation"),
("info.info-pf","informatique/Performance et fiabilité"),
("info.info-pl","informatique/Langage de programmation"),
("info.info-rb","informatique/Robotique"),
("info.info-ro","informatique/Recherche opérationnelle"),
("info.info-sc","informatique/Calcul formel"),
("info.info-sd","informatique/Son"),
("info.info-se","informatique/Génie logiciel"),
("info.info-si","informatique/Réseaux sociaux et d'information"),
("info.info-sy","informatique/Systèmes et contrôle"),
("info.info-ti","informatique/Traitement des images"),
("info.info-ts","informatique/Traitement du signal et de l'image"),
("info.info-tt","informatique/Traitement du texte et du document"),
("info.info-wb","informatique/Web"),
)

class Bibtex2halForm(forms.Form): 
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prénom chercheur (*)")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)", initial = 490706)   
    bibtex_file = forms.CharField(required=True, label="contenu bibtex", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)


class Bibformat2halForm(forms.Form):
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prenom chercheur (*)")
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")
    login_user = forms.CharField(required=True, max_length=40, label="Login HAL chercheur (*)")
    #choice_source = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_SOURCE_BIBTEX, label="Choix de la source", initial='DBLP' )
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)", initial = 490706)
    login_depot = forms.CharField(required=True, max_length=40, label="Login HAL référent (*)")
    passwd_depot = forms.CharField(required=True, max_length=40, label=("Password HAL référent (*)"), widget=forms.PasswordInput())
    choice_depot = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_DEPOT, label="Choix du dépôt", initial='NODEPOT' )
    mail_reponse = forms.CharField(required=True, max_length=40, label="Adresse mail de retour résultats")
    domains = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES_DOMAINS, label="Domaines de recherche", initial='math',required=False)
    domains2 = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES_DOMAINS2, label="Autres domaines", initial='info.info-gt',required=False)

class VerifHalConfForm(forms.Form): 
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prénom chercheur (*)")
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")

class BibtexXmlForm(forms.Form): 
    bib_name_user = forms.CharField(required=True, max_length=40, label="Forme auteur DBLP (*)")
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prénom chercheur (*)")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)", initial = 490706)   
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")
    login_user = forms.CharField(required=True, max_length=40, label="Login HAL chercheur (*)")

    choice_source = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_SOURCE_BIBTEX, label="Choix de la source", initial='DBLP' )

    domain1 = forms.CharField(required=True, max_length=20, label="Domaine 1 (*)")
    domain2 = forms.CharField(required=False, max_length=20, label="Domaine 2")
    domain3 = forms.CharField(required=False, max_length=20, label="Domaine 3")
    domain4 = forms.CharField(required=False, max_length=20, label="Domaine 4")
    domain5 = forms.CharField(required=False, max_length=20, label="Domaine 5")

    bibtex_file = forms.CharField(required=True, label="contenu bibtex", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)




class Csv2halForm(forms.Form):
    firstname_user = forms.CharField(required=True, max_length=40, label="prenom chercheur (*)")
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)")   
    csv_file = forms.CharField(required=True, label="contenu CSV", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)

class Csvformat2halForm(forms.Form):
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")
    login_user = forms.CharField(required=True, max_length=40, label="Login HAL chercheur (*)")
    #choice_source = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_SOURCE_BIBTEX, label="Choix de la source", initial='DBLP' )
    pubmed = forms.CharField(required=False, max_length=40, label="Pubmed", initial = "userpubmed")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)", initial = 490706)
    login_depot = forms.CharField(required=True, max_length=40, label="Login HAL référent (*)")
    passwd_depot = forms.CharField(required=True, max_length=40, label=("Password HAL référent (*)"), widget=forms.PasswordInput())
    choice_depot = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_DEPOT, label="Choix du dépôt", initial='NODEPOT' )
    mail_reponse = forms.CharField(required=True, max_length=40, label="Adresse mail de retour résultats")
    domains = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES_DOMAINS, label="Domaines de recherche", initial='math',required=False)
    domains2 = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=CHOICES_DOMAINS2, label="Autres domaines", initial='info.info-gt',required=False)


class SearchDataHalForm(forms.Form):
    mail_response = forms.CharField(required=True, max_length=40, label="Adresse mail de retour résultats")
    annee_debut = forms.IntegerField(required=True, label="Année de début de la recherche", initial = 2015)
    annee_fin = forms.IntegerField(required=True, label="Année de fin de la recherche", initial = 2022)
    users_hal = forms.CharField(required=True, label="Liste utilisateurs", widget=forms.Textarea(attrs={'rows':5, 'cols':80}),)


class Text2halForm(forms.Form): 
    form_author = forms.CharField(required=True, max_length=40, label="Forme auteur (*)")
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prénom chercheur (*)")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)")   
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")
    login_user = forms.CharField(required=True, max_length=40, label="Login HAL chercheur (*)")
    
    login_depot = forms.CharField(required=True, max_length=40, label="Login HAL référent (*)")
    passwd_depot = forms.CharField(required=True, max_length=40, label=("Password HAL référent (*)"), widget=forms.PasswordInput())
    choice_depot = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_DEPOT, label="Choix du dépôt", initial='NODEPOT' )

    domain1 = forms.CharField(required=True, max_length=20, label="Domaine 1 (*)")
    domain2 = forms.CharField(required=False, max_length=20, label="Domaine 2")
    domain3 = forms.CharField(required=False, max_length=20, label="Domaine 3")
    domain4 = forms.CharField(required=False, max_length=20, label="Domaine 4")
    domain5 = forms.CharField(required=False, max_length=20, label="Domaine 5")

    text_file = forms.CharField(required=True, label="contenu texte", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)


class TextXmlForm(forms.Form): 
    form_author = forms.CharField(required=True, max_length=40, label="Forme auteur (*)")
    name_user = forms.CharField(required=True, max_length=40, label="Nom chercheur (*)")
    firstname_user = forms.CharField(required=True, max_length=40, label="Prénom chercheur (*)")
    labo_auth_final = forms.CharField(required=True, max_length=40, label="N° de structure (*) (Ex: 490706)")   
    id_hal_user = forms.CharField(required=True, max_length=40, label="IdHal chercheur (*)")
    login_user = forms.CharField(required=True, max_length=40, label="Login HAL chercheur (*)")

    domain1 = forms.CharField(required=True, max_length=20, label="Domaine 1 (*)")
    domain2 = forms.CharField(required=False, max_length=20, label="Domaine 2")
    domain3 = forms.CharField(required=False, max_length=20, label="Domaine 3")
    domain4 = forms.CharField(required=False, max_length=20, label="Domaine 4")
    domain5 = forms.CharField(required=False, max_length=20, label="Domaine 5")

    text_file = forms.CharField(required=True, label="contenu texte", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)

class ModifTextForm(forms.Form):
    delete_char = forms.IntegerField(required=False, label="Supprimer premiers caracteres", initial = 0)
    add_begin = forms.CharField(required=False, max_length=20, label="Ajouter lettres + chiffre + -")
    clean_text = forms.BooleanField(required=False, label="Nettoyer et formater", initial=False)
    reverse_first_lastname = forms.BooleanField(required=False, label="Inverser nom et prénom", initial=False)
    get_volume = forms.BooleanField(required=False, label="Formater les volumes", initial=False)
    get_pages = forms.BooleanField(required=False, label="Formater les pages", initial=False)
    transform_country = forms.BooleanField(required=False, label="Formater les pays", initial=False)
    text_modif = forms.CharField(required=True, label="contenu texte", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)

class TextFromRG(forms.Form):
    choice_source = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES_SOURCE_TEXT, label="Choix de la source", initial='RG' )    
    text_modif = forms.CharField(required=True, label="contenu texte", widget=forms.Textarea(attrs={'rows':20, 'cols':90}),)


