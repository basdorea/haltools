
from __future__ import unicode_literals
import requests

from celery import shared_task

# lib XML
from lxml import etree

# lib csv
#try:
    # for Python 2.x
#from StringIO import StringIO
#except ImportError:
    # for Python 3.x
#    from io import StringIO
import csv
#import unicodecsv

from .create_xml2hal import createXml_sendHal, create_single_xml

#################
## VARIABLES
#################


# Fichier CSV en sortie
file_publis_csv = "all_csv.csv"


"""
def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, 'utf-8') for cell in row]
"""

########################################################################################################################################
########################################################################################################################################
########## SCRIPT PRINCIPAL
########################################################################################################################################
########################################################################################################################################
@shared_task
def script_csv_2_hal(name_user, firstname_user, labo_auth_final, csv_source, csv_result):
    print ("script_csv_2_hal")
    existe = ""
    problemes_url = ""    
    problemes_doublon = ""
    a_deposer = ""

    ###################################
    ###### COMPTEURS
    ###################################

    cnt_article = 0
    cnt_inproceeding = 0
    cnt_proceeding = 0
    cnt_incollection = 0
    cnt_book = 0
    cnt_total = 0
    cnt_phdthesis = 0

    cnt_depot_D = 0
    cnt_depot_E = 0
    cnt_depot_A = 0
    cnt_depot_P = 0
    cnt_depot_2 = 0


    # errors bibtex
    cnt_error_auth = 0
    cnt_error_title = 0
    cnt_error_jrn = 0
    cnt_error_vol = 0
    cnt_error_numb = 0
    cnt_error_pages = 0
    cnt_error_year_art = 0
    cnt_error_doi = 0
    cnt_error_booktitle = 0
    cnt_error_pages = 0
    cnt_error_year_inp = 0
    cnt_error_crossref = 0
    cnt_error_publisher = 0
    cnt_error_editor = 0
    cnt_error_idcrossref = 0
    cnt_error_publisher_p = 0
    cnt_error_editor_p = 0

    list_pub_csv = []

    with open('pubmed_sources/user_2404_123456.csv', 'r') as csvr:
        csvreader = csv.reader(csvr, delimiter=',')
        #csvwriter = csv.writer(csvf, delimiter=',')
        next(csvreader)
        for row in csvreader :
            print("{0} - {1}".format(row[0], row[1]))


    return None


def script_csv_2_hal_old (csv_file, form_author, name_user, firstname_user, labo_auth_final, id_hal_user, login_user, listdomains, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod, single, source_csv):
    '''
    take csv file and some values in entry and parse the bibtex to get info upon publications

    if the bool 'single' == False -> bibtex file has many publications, call createXml_sendHal (create XML and deposit) return a lis of results    
    if the bool 'single' == True -> bibtex file has one publication, call create_single_xml (post single XML no deposit) return the XML in string
    '''
    print ("script_csv_2_hal bool_depot_prod ",bool_depot_prod)
    resultat = ""
    problemes_url = ""    
    problemes_doublon = ""
    problemes_depot = ""    
    depots = ""
    reponse_single_xml = ""

    list_pub_csv=[]

    print ("source CSV "+source_csv)

    ###################################
    ###### COMPTEURS
    ###################################

    cnt_article = 0
    cnt_conf = 0
    cnt_book = 0
    cnt_total = 0
    cnt_reel = 0
    cnt_phdthesis = 0

    # errors bibtex
    cnt_error_auth = 0
    cnt_error_title = 0
    cnt_error_jrn = 0
    cnt_error_vol = 0
    cnt_error_numb = 0
    cnt_error_pages = 0
    cnt_error_year_art = 0
    cnt_error_doi = 0
    cnt_error_booktitle = 0
    cnt_error_pages = 0
    cnt_error_year_inp = 0
    cnt_error_crossref = 0
    cnt_error_publisher = 0
    cnt_error_editor = 0
    cnt_error_idcrossref = 0
    cnt_error_publisher_p = 0
    cnt_error_editor_p = 0


    # read CSV contries
    list_acronym_country = []
    with open('hal/countries.csv', 'rb') as csvfile:
        delim = str(':')
        quotech = str('|')
        list_countries = csv.reader(csvfile, delimiter=delim, quotechar=quotech)
        for row in list_countries:
            list_acronym_country.append((row[1],row[0]))



    # Split CSV
    list_all_pub = []
    list_all_pub = csv_file.split("\r\n")
    
    #del list_pub_csv[0]
    for row_pub in list_all_pub :
        cnt_total+=1

        # initialize entries
        type_publi =""
        pubmed = ""
        numero = ""
        language = "en"
        title = ""
        confjournal=""
        nb_pages=""
        volume = ""
        town=""
        country=""
        country_acr="XX"
        year = 0
        doi_value = ""
        pubmed = ""
        publisher_book = ""
        editor_book = ""

        # split row publi
        list_field = []
        try :
            list_field = row_pub.split("_")
        except AttributeError :
            list_field = []
        
        if len(list_field) > 0 :
            cnt_reel+=1

            ################
            ### IF PUBMED
            if source_csv == "PUBMED":
                title = list_field[0]
                #print ("title "+title)

                
                authors = list_field[2]
                details = list_field[3]
                confjournalyear = list_field[4]
                typepub = list_field[6]
                pubmed = list_field[9]
                
                ## TYPEPUB
                if typepub == "citation" :
                    cnt_article+=1
                    type_publi = "ART"
                    numero = "RI"+str(cnt_article)
                ## TODO -> mettre autres
                print ("numero "+str(numero))
                print ("pubmed "+str(pubmed))

                ## AUTHORS
                try :
                    authors= authors.replace(", et al.","")
                    x = authors.find(";")
                    authors = authors[:x]
                    listauthors = []
                    list_authors_mix = authors.split(",")
                    for auth in list_authors_mix :
                        auth = auth.strip()
                        lab_struct = ""
                        #print ("TEST form_author "+form_author+" - auth "+auth)

                        if form_author in auth :
                            print ("Script_csv Match form_author keynote")
                            auth = name_user+" "+firstname_user
                            lab_struct = labo_auth_final

                        auth_full = auth.split(" ")
                        prenom = auth_full[1]
                        prenom = prenom.encode('utf-8')
                        nom = auth_full[0]
                        nom = nom.encode('utf-8')
                        #print ("author "+str(nom)+ " - prenom "+str(prenom)+" - labo"+str(lab_struct))
                        #print ("Script_dblp "+"author "+auth.encode('utf-8')+ " - prenom "+ prenom.encode('utf-8')+ " - nom "+nom.encode('utf-8')+ str(type(auth.encode('utf-8'))))
                        listauthors.append((nom,prenom,lab_struct))
                except IndexError :
                    print ('listauthors pb')
                    #listauthors = []

                ## CONF JOURNAL YEAR
                x = confjournalyear.find(".")
                confjournal = confjournalyear[:x]
                year = confjournalyear[x+1:]
                year = year.strip()

                #print ("confjournal "+str(confjournal))
                #print ("year "+str(year))

                ## VOL PAGES
                # split details
                x = details.find("pii: ")
                if x == -1 : # get vol and pages
                    y = details.find(";")
                    if y == -1 :
                        volume =""
                        nb_pages = ""
                    else :
                        volpages = details[y+1:]
                        z = volpages.find(":")
                        volume = volpages[:z]
                        nb_pages = volpages[z+1:]
                        w = nb_pages.find(".")
                        nb_pages = nb_pages[:w]

                else :
                    volume=""
                    nb_pages = ""

                print ("vol "+str(volume)+ " - pages "+str(nb_pages))

                ## DOI
                x = details.find("doi: ")
                if x == -1 : # no doi
                    doi_value = ""
                else :
                    doi_value = details[x+5:]
                    y = doi_value.find(" ")
                    if y > -1 :
                        doi_value = doi_value[:y-1]
                    else :
                        doi_value = doi_value[:-1]  #suppression point final
                    

                print ("doi "+str(doi_value))
                ########################
                ### ENDIF PUBMED

            
            # Test value "single" 
            # if false -> call createXml_sendHal (create XML and deposit)
            # if true -> call create_single_xml (build XML without creation, no deposit)
            if (type_publi == "ART") or (type_publi == "COMM") or (type_publi == "OUV") :
                if single == False :

                    action_todo=""

                    # Verification que la publi n'existe pas deja dans HAL
                    # pour un meilleur matching, titre en minuscule et recherche par le champ title_t (sans casse ni accent)
                    title_low = title.lower()
                    url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(title_low)

                    req = requests.get(url_request)       
                    json = ""
                    try :
                        json = req.json()
                    except ValueError :
                        print ("PROBLEME VALUEERROR")
                    try : 
                        if json is not "" :
                            result = json['response']['docs']

                            # si un resultat est trouve, recup authors et URI pour affichage dans 'resultat', action_todo = "E"
                            if (len(result) == 1 ) :
                                all_auth = ""
                                try :
                                    tous_authors = result[0]["authFullName_s"]
                                    for auth in tous_authors:
                                        all_auth = all_auth + auth+"-"
                                except KeyError as ke :
                                    print ("error print authors existing publi keyerror {0}".format(ke))
                                resultat = resultat + "num. "+numero+" - "+result[0]["uri_s"]+" - auteurs:"+all_auth+"<br/>"
                                action_todo = "E"

                            # si plusieurs resultats trouves, recup URI pour affichage dans 'problemes_doublon', action_todo = "2"
                            if (len(result) >1 ) :
                                problemes_doublon = problemes_doublon + "num. "+numero+" - URLS "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"<br/>"
                                action_todo = "2"

                            # Si aucun resultat on peut deposer, action_todo = "D"
                            if (len(result) == 0 ) :
                                action_todo = "D"
                                result = False
                                accept_depot = True

                                # Si caracteres incoherents (issus de DBLP) dans le titre -> pas de depot -> problemes_depot
                                if ("\\(^\\mbox" in title) :
                                    print("-----------------> MBOX")
                                    accept_depot = False
                                    result = False

                                title = title.encode("utf-8")
                                confjournal = confjournal.encode("utf-8")
                                town = town.encode("utf-8")
                                editor_book = editor_book.encode("utf-8")

                                if accept_depot == True :
                                    print ("bool_depot_prod ",bool_depot_prod )
                                    result = createXml_sendHal(numero, listauthors, language, title, confjournal, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod)

                                # methode createXml_sendHal renvoie true -> depot HAL ou preprod OK
                                if result == True :
                                    depots = depots + "num. "+numero+" au titre "+title.decode("utf-8")+" deposee dans HAL<br/>"
                                # methode createXml_sendHal renvoie true -> pb depot HAL ou preprod ou pas de depot demande    
                                if result == False :
                                    problemes_depot = problemes_depot + "num. "+numero+" au titre "+title.decode("utf-8")+" a un probleme de depot<br/>"
                        else :
                            # pb de lecture json, pas depot -> problemes_url
                            action_todo = "P"
                            problemes_url = problemes_url + "num. "+numero+" au titre "+title.decode("utf-8")+"<br/>"                        
                    except KeyError :
                        # pb de lecture json, pas depot -> problemes_url
                        action_todo = "P"
                        problemes_url = problemes_url + "num. "+numero+" au titre "+title.decode("utf-8")+"<br/>"

                    # Creation du CSV qui sera renvoye et affiche
                    authors = authors.replace(" and",", ")
                    list_pub_csv.append((numero,authors,title,confjournal,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))


                elif single == True :
                    title = title.encode("utf-8")
                    confjournal = confjournal.encode("utf-8")
                    town = town.encode("utf-8")
                    editor_book = editor_book.encode("utf-8")                
                    login_user = login_depot
                    reponse_single_xml = create_single_xml(listauthors, language, title, confjournal, nb_pages, year, listdomains, type_publi, town, country, country_acr, doi_value, editor_book, volume, name_user, labo_auth_final, id_hal_user, login_user)
        else :
            print("empty line")    
        # ------------------END LOOP -----------------
        



    ########################
    ####### ECRITURE RESULTATS  -> list_resultats
    print ("list_pub_csv length :",cnt_total)
    
    list_resultats=[]
    for pub in list_pub_csv :
        allauth = pub[1]
        allauth = allauth.replace("\n","")
        title = pub[2]#.decode("utf-8") 
        confjournal = pub[3]#.decode("utf-8") 
        ville = pub[9]#.decode("utf-8")

        list_resultats.append((str(pub[0]),allauth,title,confjournal,str(pub[4]),str(pub[5]),str(pub[6]),str(pub[7]),str(pub[8]),ville,str(pub[10]),str(pub[11]),str(pub[12])))
  
    cnt_nb_publis = cnt_article + cnt_conf + cnt_book + cnt_phdthesis

    list_resultats.append(("RESULTATS","nombre de publis",str(cnt_nb_publis),"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","publis deja presentes dans HAL",resultat,"","","","","","","","","",""))
    list_resultats.append(("RESULTATS","depots",depots,"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","problemes de depot",problemes_depot,"","","","","", "","","","",""))
    list_resultats.append(("RESULTATS","problemes de doublon",problemes_doublon,"","","","", "","","","","","")) 
    list_resultats.append(("RESULTATS","problemes url",problemes_url,"","","","","","","","","","")) 

    print ("####### RESULTATS PARSING BIBTEX ########")
    print ("cnt_article ", cnt_article)
    print ("cnt_conf ", cnt_conf)
    print ("cnt_book ", cnt_book)
    print ("cnt_phdthesis ", cnt_phdthesis)
    print ("cnt_reel ", cnt_reel)    
    print ("cnt_total ", cnt_total)

    print ("ERROR Author", cnt_error_auth)
    print ("ERROR Title", cnt_error_title)

    """
    print ("-------ERRORS ARTICLE------")
    print ("cnt_error_jrn",cnt_error_jrn)
    print ("cnt_error_vol",cnt_error_vol)
    print ("cnt_error_numb",cnt_error_numb)
    print ("cnt_error_pages",cnt_error_pages)
    print ("cnt_error_year_art",cnt_error_year_art)
    print ("cnt_error_doi",cnt_error_doi)

    print ("-------ERRORS INPROCEEDINGS------")
    print ("cnt_error_booktitle:",cnt_error_booktitle)
    print ("cnt_error_pages:",cnt_error_pages)
    print ("cnt_error_year_inp:",cnt_error_year_inp)
    print ("cnt_error_crossref:",cnt_error_crossref)
    print ("cnt_error_publisher:",cnt_error_publisher)
    print ("cnt_error_editor:",cnt_error_editor)

    print ("-------ERRORS PROCEEDINGS------")
    print ("cnt_error_idcrossref:",cnt_error_idcrossref)

    print ("#########################################")
    """
    print ("######## RESULTATS XML + DEPOTS ##########")
    print ("RESULTATS existants")
    print (resultat.encode("utf-8"))
    print ("------------------")
    print ("DEPOTS effectues")
    print (depots.encode("utf-8"))
    print ("------------------")
    print ("PROBLEMES DEPOTS")
    print (problemes_depot.encode("utf-8"))
    print ("------------------")
    print ("PROBLEMES DOUBLONS")
    print (problemes_doublon.encode("utf-8"))
    print ("------------------")
    print ("PROBLEMES URL")
    print (problemes_url.encode("utf-8"))
    
    #print ("special_cnt"+str(special_cnt))

    if single == False :
        return list_resultats
    if single == True :
        return reponse_single_xml
    