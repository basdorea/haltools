#!/usr/bin/python

from __future__ import unicode_literals

import requests
import csv
from celery import shared_task

# lib XML
from lxml import etree

from utils import mails

import html

#############################################################################################################################
###################################           BUILD XML ARTICLE CONF POSTER OUV COUV
# CREATION XML pour articles, conferences et posters, ouvrage et chapitres d'ouvrage
#############################################################################################################################
#############################################################################################################################

def createXml_sendHal(numero,listauthors, lang_title, title_publi, name_conf, nb_pages, date_pub, listdomains, type_pub, ville, pays, pays_acr, doi_value, editor_book, volume, pubmed, firstname_user, name_user, labo_auth_final, id_hal_user, login_user, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod):
    print ("method createXml_sendHal begin")
    ## VARIABLES CREATION XML
    errormsg=""

    # Recup des donnes de la liste
    namefile = "xml_files/"+login_depot+str(numero)+".xml"

    file = open(namefile,"w") 
    file.write("<?xml version=\"1.0\" encoding=\"latin1\"?>\n")

    tei = etree.Element("TEI")
    tei.set("xmlns","http://www.tei-c.org/ns/1.0")
    tei.set("xmlns_hal","http://hal.archives-ouvertes.fr/")   ### ATTENTION   remplacement xmlns_hal par xmlns:hal

    text = etree.SubElement(tei, "text")
    body = etree.SubElement(text, "body")
    listBibl = etree.SubElement(body, "listBibl")
    biblFull = etree.SubElement(listBibl, "biblFull")

    # TitleStmt
    if (type_pub == "COMM") or (type_pub == "POSTER") or  (type_pub == "ART") or (type_pub == "OUV") or  (type_pub == "COUV")  :

        titleStmt = etree.SubElement(biblFull, "titleStmt")
        title = etree.SubElement(titleStmt, "title")
        title.set("xml_lang",lang_title)        #Build from data      ### ATTENTION  remplacement xml_lang par xml:lang

        title.text = title_publi      #Build from data


        #print ("CREATE XML title "+str(type(title_publi))+ " title UTF8 "+title_publi.decode("latin1")+str(type(title_publi.decode("latin1"))))
        ## TEST
        print ("createXML, id_hal user ",id_hal_user)
        listauthors = listauthors.split(',')
        for auth_all in listauthors :
            auth_all = auth_all.strip()
            print("auth_all {0}".format(auth_all))
            
            if (firstname_user in auth_all) and (name_user in auth_all) :
                auth = auth_all.split(' ')
                prenom_auth = firstname_user
                nom_auth = name_user
            else :
                auth = auth_all.split(' ')
                if len(auth) != 2 :
                    errormsg+="Erreur probable sur le nom {0}, ".format(auth_all)
                prenom_auth = auth[0]
                nom_auth = auth[-1]
            print("nom_auth {0} prenom_auth {1}".format(nom_auth, prenom_auth))
            author = etree.SubElement(titleStmt, "author")
            author.set("role","aut")
            persName = etree.SubElement(author, "persName")
            forename = etree.SubElement(persName, "forename")
            forename.set("type","first")
            forename.text = prenom_auth                #Build from data
            surname = etree.SubElement(persName, "surname")

            surname.text = nom_auth     
            #print ("AUTH 2 {0} {1} {2} ".format(nom_auth,prenom_auth,auth[2]))                #Build from data
            labstruct=""
            if name_user in auth :
                labstruct=labo_auth_final
            if len(labstruct) > 1 :
                if nom_auth == name_user and id_hal_user is not "" :
                    idno =  etree.SubElement(author, "idno")
                    idno.set("type", "idhal")
                    idno.text = id_hal_user
                affiliation = etree.SubElement(author, "affiliation")
                labo_author = "#"+labstruct
                affiliation.set("ref",labo_author)             #Build from data



    # Unnecessary noteStmt
    """
    notesStmt = etree.SubElement(biblFull, "notesStmt")
    note = etree.SubElement(notesStmt, "note")
    note.set("type","commentary")
    note.text = comment_publi
    """
    # sourceDesc
    sourceDesc = etree.SubElement(biblFull, "sourceDesc")
    biblStruct = etree.SubElement(sourceDesc, "biblStruct")
    analytic = etree.SubElement(biblStruct, "analytic")
    title = etree.SubElement(analytic, "title")
    title.set("xml_lang",lang_title)          #Build from data             ### ATTENTION remplacement  xml_lang par xml:lang
    title.text = title_publi                  #Build from data

    for auth_all in listauthors :
        auth_all = auth_all.strip()
        
        
        if (firstname_user in auth_all) and (name_user in auth_all) :
            auth = auth_all.split(' ')
            prenom_auth = firstname_user
            nom_auth = name_user
        else :
            auth = auth_all.split(' ')
            if len(auth) != 2 :
                errormsg+="Erreur probable sur le nom {0}, ".format(auth_all)
            prenom_auth = auth[0]
            nom_auth = auth[-1]
        
        #auth = auth_all.split(' ')
        #nom_auth = auth[0].strip()
        #nom_auth = nom_auth
        #prenom_auth = auth[1].strip()
        #prenom_auth = prenom_auth
        author = etree.SubElement(analytic, "author")
        author.set("role","aut")
        persName = etree.SubElement(author, "persName")
        forename = etree.SubElement(persName, "forename")
        forename.set("type","first")
        forename.text = prenom_auth               #Build from data
        surname = etree.SubElement(persName, "surname")
        surname.text = nom_auth                    #Build from data
        labstruct=""
        if name_user in auth :
            labstruct=labo_auth_final
        if len(labstruct) > 1 :
            if nom_auth == name_user and id_hal_user is not "":
                idno =  etree.SubElement(author, "idno")
                idno.set("type", "idhal")
                idno.text = id_hal_user
            affiliation = etree.SubElement(author, "affiliation")
            labo_author = "#"+labstruct
            affiliation.set("ref",labo_author)             #Build from data


    monogr = etree.SubElement(biblStruct, "monogr")
    title = etree.SubElement(monogr, "title")
    if type_pub == "ART":
        title.set("level","j")
        title.text = name_conf                      #Build from data
    if (type_pub == "COMM") or (type_pub == "POSTER"):
        title.set("level","m")
        title.text = name_conf                   #Build from data 
    # Pour les COUV on donne le titre du livre (ici noté name_conf)
    if type_pub == "COUV":
        title = etree.SubElement(monogr, "title")
        title.set("level","m")
        title.text = name_conf   # ici name_conf = titre livre                    #Build from data
        if len(editor_book) > 0 :
            editor = etree.SubElement(monogr, "editor")
            editor.text = editor_book     #Build from data

    if (type_pub == "COMM") or (type_pub == "POSTER") :
        if (pays.strip() is not ""):
            meeting = etree.SubElement(monogr, "meeting")
            title = etree.SubElement(meeting, "title")
            title.text = name_conf                    #Build from data

            date = etree.SubElement(meeting, "date")
            date.set("type","start")
            date.text = date_pub
            settlement = etree.SubElement(meeting, "settlement")
            settlement.text = ville
            country = etree.SubElement(meeting, "country")
            country.set("key",pays_acr)
            country.text = pays
    
    imprint = etree.SubElement(monogr, "imprint")
    
    if len(volume) > 0 :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","volume")
        biblScope.text = volume
    if len(nb_pages) > 0 :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","pp")
        biblScope.text = nb_pages
    
    date = etree.SubElement(imprint, "date")
    date.set("type","datePub")
    date.text = date_pub

    if len(doi_value) > 0 :
        doi = etree.SubElement(biblStruct, "idno")
        doi.set("type","doi")
        doi.text = doi_value

    if len(pubmed) > 0 :
        pubm = etree.SubElement(biblStruct, "idno")
        pubm.set("type","pubmed")
        pubm.text = pubmed

    # profileDesc
    profileDesc = etree.SubElement(biblFull, "profileDesc")
    langUsage = etree.SubElement(profileDesc, "langUsage")
    language = etree.SubElement(langUsage, "language")
    language.set("ident",lang_title)                     #Build from data
    textClass = etree.SubElement(profileDesc, "textClass") 
    for domain in listdomains :
        classCode = etree.SubElement(textClass, "classCode") 
        classCode.set("scheme","halDomain")
        classCode.set("n",domain)                          #Build from data
    classCode = etree.SubElement(textClass, "classCode") 
    classCode.set("scheme","halTypology")
    classCode.set("n",type_pub)

    # back 
    back = etree.SubElement(text, "back")
    listOrg = etree.SubElement(back, "listOrg")
    listOrg.set("type","structures")
    org = etree.SubElement(listOrg, "org")
    org.set("type","laboratory")
    org.set("xml_id",labo_auth_final)                                            ### ATTENTION remplacement  xml_id par xml:id

    docxml = etree.tostring(tei, pretty_print=True)

    file.write(str(docxml))

    file.close() 

    ## CORRECTIFS

    with open(namefile, 'r') as file :
        docxml = file.read()

    # Replace the target string
    docxml = docxml.replace("xmlns_hal=","xmlns:hal=")
    docxml = docxml.replace("xml_id=","xml:id=")
    docxml = docxml.replace("xml_lang=","xml:lang=")
    docxml = docxml.replace("b'<TEI","<TEI")
    docxml = docxml.replace("\\n'","")
    docxml = docxml.replace("\\n","\n")
    #docxml = docxml.replace('<?xml version="1.0" encoding="utf-8"?>','')
    #docxml = docxml.replace("b'<?xml version=\'1.0\' encoding=\'latin1\'?>","<?xml version='1.0' encoding='latin1'?>")
    #docxml = str(docxml)
    #print(docxml)
    print(docxml)
    
    # Write the file out again
    with open(namefile, 'w', encoding="utf8") as file:
        file.write(docxml)

    # HEADERS request 
    headers = {
        'Packaging': 'http://purl.org/net/sword-types/AOfr',
        'Content-Type': 'text/xml',
        'On-Behalf-Of': 'login|'+login_user+';idhal|'+id_hal_user,
    }

    #reponse_http = False
    #response = ""

    code_http = 0
    requ_msg = ""

    print ("createXML bool_depot_prod {0}".format(bool_depot_prod))
    # DEPOT PREPROD
    if bool_depot_preprod == True :
        #namefile = "xml_files2/bdoreauRI1.xml"
        data = open(namefile)
        try :
            response = requests.post('https://api-preprod.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot),timeout=60)
        except requests.exceptions.RequestException as e:
            print ("ERROR REQUEST {0}".format(e))
            requ_msg += "ERROR REQUEST : {0} -- ".format(e)
        except requests.exceptions.Timeout as t :
            print ("ERROR TIMEOUT REQUEST {0}".format(t))
            requ_msg += "ERROR TIMEOUT : {0} -- ".format(t)

        print("response POST : code {0}".format(response.status_code))
        print("response POST : text {0}".format(response.text))
        code_http = response.status_code
        if (code_http != 200) and (code_http != 202) :
            requ_msg += "RESPONSE : {0}".format(response.text)
        else :
            requ_msg += "RESPONSE : OK  {0}".format(errormsg)

    # DEPOT PROD
    if bool_depot_prod == True :
        data = open(namefile)
        try :
            response = requests.post('https://api.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot),timeout=60)
        except requests.exceptions.RequestException as e:
            print ("ERROR REQUEST {0}".format(e))
        except requests.exceptions.Timeout as t :
            print ("ERROR TIMEOUT REQUEST {0}".format(t)) 

        print("response POST : code {0}".format(response.status_code))
        print("response POST : text {0}".format(response.text))
        code_http = response.status_code
        if (code_http != 200) and (code_http != 202) :
            requ_msg += "RESPONSE : {0}".format(response.text)
        else :
            requ_msg += "RESPONSE : OK  {0}".format(errormsg)
    
    reponse = (code_http, requ_msg)
    return reponse


####################################################################
########## END METHOD CREATE XML
##########
####################################################################

@shared_task
def sendselect_2hal(firstname_user, name_user, login_user,id_hal_user,mail_reponse,labo_auth_final,listdomains,login_depot,passwd_depot,choice_depot,list_publis_to_update,csvfile):
    print("list_publis_to_update")
    print(list_publis_to_update)
    mail_message = "Bonjour\n\nSuite à votre demande sur l'appli Haltools, veuillez trouver ci-dessous les résultats de dépôts.\n\n"
    with open('bibtex_csv_files/'+csvfile+'.csv', 'r', newline='', encoding='utf-8') as csv_file :
        csvr = csv.reader(csv_file, delimiter =',')
        for row in csvr :
            numero = row[0]
            listauthors = row[1]
            title_publi = row[2]
            name_conf = row[3]
            nb_pages = row[4]
            volume = row[5]
            date_pub = row[6]
            type_pub = row[7]
            ville = row[9]
            pays = row[10]
            pays_acr = row[11]
            doi_value = row[12]
            editor_book = row[13]
            lang_title = row[14]

            pubmed = ""

            bool_depot_preprod = False
            bool_depot_prod = False
            mailchoicedepot = ""

            if choice_depot == "PREPROD" :
                bool_depot_preprod = True
                mailchoicedepot = "sur la preprod de HAL"
            
            if choice_depot == "PRODHAL" :
                bool_depot_prod = True
                mailchoicedepot = "sur HAL"

            if numero in list_publis_to_update : # ie publi selected 
                # encodage

                rep = (600,'Erreur lors de la creation du XML')
                print("createXml_sendHal pour {0}".format(numero))
                rep = createXml_sendHal(numero,listauthors, lang_title, title_publi, name_conf, nb_pages, date_pub, listdomains, type_pub, ville, pays, pays_acr, doi_value, editor_book, volume, pubmed, firstname_user, name_user, labo_auth_final, id_hal_user, login_user, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod)
                print("Resultat pour {0} -> code : {1} - requ msg : {2}".format(numero,rep[0],rep[1]))
                if (int(rep[0]) == 200) or (int(rep[0]) == 202) :
                    mail_message+="La publi au numéro {0} et au titre {1} publiée en {2} a bien été déposée {3}\n".format(numero, title_publi, date_pub, mailchoicedepot)
                    mail_message+="{0}\n\n".format(rep[1])
                else :
                    mail_message+="La publi au numéro {0} et au titre {1} publiée en {2} n'a pas pu être déposée {3}\n".format(numero, title_publi, date_pub, mailchoicedepot)
                    mail_message+="L'erreur remontée est {0}\n\n".format(rep[1])
    mail_message+="Cordialement\nL'équipe Haltools"
    mails.sendonemail("[HALTOOLS] résultats dépôts",mail_message,mail_reponse)
    return None


















def create_single_xml(listauthors, lang_title, title_publi, name_conf, nb_pages, date_pub, listdomains, type_pub, ville, pays, pays_acr, doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user):
    print ("method create_single_xml begin")
    data = ""

    ## VARIABLES CREATION XML

    # Recup des donnes de la liste
    namefile = "xml_files/"+str(login_user)+"temp.xml"

    file = open(namefile,"w") 
    file.write("<?xml version=\"1.0\" encoding=\"latin1\"?>\n")


    tei = etree.Element("TEI")
    tei.set("xmlns","http://www.tei-c.org/ns/1.0")
    tei.set("xmlns_hal","http://hal.archives-ouvertes.fr/")   ### ATTENTION   remplacement xmlns_hal par xmlns:hal

    text = etree.SubElement(tei, "text")
    body = etree.SubElement(text, "body")
    listBibl = etree.SubElement(body, "listBibl")
    biblFull = etree.SubElement(listBibl, "biblFull")

    # TitleStmt
    if (type_pub == "COMM") or (type_pub == "POSTER") or  (type_pub == "ART") or (type_pub == "OUV") or  (type_pub == "COUV")  :

        titleStmt = etree.SubElement(biblFull, "titleStmt")
        title = etree.SubElement(titleStmt, "title")
        title.set("xml_lang",lang_title)          #Build from data      ### ATTENTION  remplacement xml_lang par xml:lang
        title.text = title_publi.decode("latin1")                   #Build from data

        for auth in listauthors :
            author = etree.SubElement(titleStmt, "author")
            author.set("role","aut")
            persName = etree.SubElement(author, "persName")
            forename = etree.SubElement(persName, "forename")
            forename.set("type","first")
            forename.text = auth[1].decode("latin1")                #Build from data
            surname = etree.SubElement(persName, "surname")
            surname.text = auth[0].decode("latin1")                    #Build from data
            if len(auth[2]) > 1 :
                if auth[0].decode("latin1") == name_user and id_hal_user is not "":
                    idno =  etree.SubElement(author, "idno")
                    idno.set("type", "idhal")
                    idno.text = id_hal_user
                affiliation = etree.SubElement(author, "affiliation")
                labo_author = "#"+auth[2]
                affiliation.set("ref",labo_author)             #Build from data


    # Unnecessary noteStmt
    '''
    notesStmt = etree.SubElement(biblFull, "notesStmt")
    note = etree.SubElement(notesStmt, "note")
    note.set("type","commentary")
    note.text = comment_publi
    '''
    # sourceDesc
    sourceDesc = etree.SubElement(biblFull, "sourceDesc")
    biblStruct = etree.SubElement(sourceDesc, "biblStruct")
    analytic = etree.SubElement(biblStruct, "analytic")
    title = etree.SubElement(analytic, "title")
    title.set("xml_lang",lang_title)          #Build from data             ### ATTENTION remplacement  xml_lang par xml:lang
    title.text = title_publi.decode("latin1")                   #Build from data
    #print ("CREATE XML title "+type(title_publi)))

    for auth in listauthors :
        author = etree.SubElement(analytic, "author")
        author.set("role","aut")
        persName = etree.SubElement(author, "persName")
        forename = etree.SubElement(persName, "forename")
        forename.set("type","first")
        forename.text = auth[1].decode("latin1")                #Build from data
        surname = etree.SubElement(persName, "surname")
        surname.text = auth[0].decode("latin1")                    #Build from data
        if len(auth[2]) > 1 :
            affiliation = etree.SubElement(author, "affiliation")
            labo_author = "#"+auth[2]
            affiliation.set("ref",labo_author)             #Build from data
            if auth[1] == name_user:
                idno_idhal =  etree.SubElement(author, "idno")
                idno_idhal.set("type", "idhal")
                idno.text = id_hal_user

    monogr = etree.SubElement(biblStruct, "monogr")
    title = etree.SubElement(monogr, "title")
    if type_pub == "ART":
        title.set("level","j")
        title.text = name_conf.decode("latin1")                       #Build from data
    if type_pub == "COMM":
        title.set("level","m")
        title.text = name_conf.decode("latin1")                       #Build from data
    if type_pub == "POSTER":
        title.set("level","m")
        title.text = name_conf.decode("latin1")    
    # Pour les COUV on donne le titre du livre (ici noté name_conf)
    if type_pub == "COUV":
        title = etree.SubElement(monogr, "title")
        title.set("level","m")
        title.text = name_conf.decode("latin1")   # ici name_conf = titre livre                    #Build from data
        if len(editor_book) > 0 :
            editor = etree.SubElement(monogr, "editor")
            editor.text = editor_book     #Build from data

    if (type_pub == "COMM") or (type_pub == "POSTER") :
        if (pays.strip() is not ""):
    
            meeting = etree.SubElement(monogr, "meeting")
            title = etree.SubElement(meeting, "title")
            title.text = name_conf.decode("latin1")                      #Build from data

            date = etree.SubElement(meeting, "date")
            date.set("type","start")
            date.text = date_pub
            settlement = etree.SubElement(meeting, "settlement")
            settlement.text = str(ville)
            country = etree.SubElement(meeting, "country")
            country.set("key",pays_acr)
            country.text = str(pays) ##.decode("latin1")
    
    imprint = etree.SubElement(monogr, "imprint")
    
    if len(volume) > 0 :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","volume")
        biblScope.text = str(volume) ##.decode("latin1")#.strip()
    if len(nb_pages) > 0 :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","pp")
        biblScope.text = str(nb_pages) ##.decode("latin1")#.strip()
    
    date = etree.SubElement(imprint, "date")
    date.set("type","datePub")
    date.text = date_pub

    if len(doi_value) > 0 :
        doi = etree.SubElement(biblStruct, "idno")
        doi.set("type","doi")
        doi.text = doi_value

    if len(pubmed) > 0 :
        pubm = etree.SubElement(biblStruct, "idno")
        pubm.set("type","pubmed")
        pubm.text = pubmed

    # profileDesc
    profileDesc = etree.SubElement(biblFull, "profileDesc")
    langUsage = etree.SubElement(profileDesc, "langUsage")
    language = etree.SubElement(langUsage, "language")
    language.set("ident",lang_title)                     #Build from data
    textClass = etree.SubElement(profileDesc, "textClass") 
    for domain in listdomains :
        classCode = etree.SubElement(textClass, "classCode") 
        classCode.set("scheme","halDomain")
        classCode.set("n",domain)                          #Build from data
    classCode = etree.SubElement(textClass, "classCode") 
    classCode.set("scheme","halTypology")
    classCode.set("n",type_pub)

    # back 
    back = etree.SubElement(text, "back")
    listOrg = etree.SubElement(back, "listOrg")
    listOrg.set("type","structures")
    org = etree.SubElement(listOrg, "org")
    org.set("type","laboratory")
    org.set("xml_id",labo_auth_final)                                            ### ATTENTION remplacement  xml_id par xml:id

    docxml = etree.tostring(tei, pretty_print=True, encoding='unicode')

    file.write(docxml) 
    file.close() 

    ## CORRECTIFS

    with open(namefile, 'r') as file :
      docxml = file.read()

    # Replace the target string
    docxml = docxml.replace("xmlns_hal=","xmlns:hal=")
    docxml = docxml.replace("xml_id=","xml:id=")
    docxml = docxml.replace("xml_lang=","xml:lang=")

    rep = ""
    # Write the file out again
    with open(namefile, 'w') as file:
        file.write(docxml)

    return docxml
