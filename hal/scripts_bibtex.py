#!/usr/bin/python
#-*- coding: latin1 -*-

from __future__ import unicode_literals

from celery import shared_task

import requests
import csv

# lib XML
from lxml import etree

# lib bibtex
import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode

from .create_xml2hal import createXml_sendHal, create_single_xml
from utils import mails

from .dict_countries import dict_countries
from haltools.settings import BASE_DIR, MEDIA_ROOT
#################
## VARIABLES
#################

resultat = ""
problemes_url = ""    
problemes_doublon = ""
a_deposer = ""
problemes_depot = ""    
depots = ""


########################################################################################################################################
########################################################################################################################################
########## SCRIPT PRINCIPAL
########################################################################################################################################
########################################################################################################################################

''' ## Plus utilise
def get_info_from_proceeding(crossref, bib_database) :
    #global bib_database
    list_info = []
    for entrycrossref in bib_database.entries :
        if entrycrossref['ENTRYTYPE']=='proceedings' :
            if entrycrossref['ID'] == crossref :
                print ("CROSSREF")
                #print ("    TITLE : ", entrycrossref['title'])
                #print ("    PUBLISHER : ", entrycrossref['publisher'])
    result = entrycrossref['title']
    return result
'''

@shared_task
def script_bibtex_2_hal (bibtex_file, name_user, firstname_user, labo_auth_final, csvfile):
    
    '''
    take bibtex file and some values in entry and parse the bibtex to get info upon publications

    if the bool 'single' == False -> bibtex file has many publications, call createXml_sendHal (create XML and deposit) return a lis of results    
    if the bool 'single' == True -> bibtex file has one publication, call create_single_xml (post single XML no deposit) return the XML in string
    '''
    print ("script_bibtex_2_hal")
    existe = ""
    problemes_url = ""    
    problemes_doublon = ""
    a_deposer = ""

    ###################################
    ###### COMPTEURS
    ###################################

    cnt_article = 0
    cnt_inproceeding = 0
    cnt_proceeding = 0
    cnt_incollection = 0
    cnt_book = 0
    cnt_total = 0
    cnt_phdthesis = 0

    cnt_depot_D = 0
    cnt_depot_E = 0
    cnt_depot_A = 0
    cnt_depot_P = 0
    cnt_depot_2 = 0


    # errors bibtex
    cnt_error_auth = 0
    cnt_error_title = 0
    cnt_error_jrn = 0
    cnt_error_vol = 0
    cnt_error_numb = 0
    cnt_error_pages = 0
    cnt_error_year_art = 0
    cnt_error_doi = 0
    cnt_error_booktitle = 0
    cnt_error_pages = 0
    cnt_error_year_inp = 0
    cnt_error_crossref = 0
    cnt_error_publisher = 0
    cnt_error_editor = 0
    cnt_error_idcrossref = 0
    cnt_error_publisher_p = 0
    cnt_error_editor_p = 0

    list_pub_csv = []

    # une participation a une conf (inproceeding) peut avoir une crossref reliee a une proceeding qui est une conf, celle ci
    # se trouve en fin du doc bibtex donc -> 
    # on cherche crossref dans inproceeding, si on le trouve aller le chercher en bas, sinon chercher editor et publier directement dans inproceeding
    # car certains chercheurs peuvent creer leur bibtex comme ceci

    #bibtex_file = bibtex_file.encode("latin1")
    #print (bibtex_file)
    
    parser = BibTexParser()
    parser.ignore_nonstandard_types = False
    parser.homogenize_fields = False
    parser.common_strings = False
    parser.customization = convert_to_unicode
    bib_database = bibtexparser.loads(bibtex_file, parser = parser)    
    '''
    parser = BibTexParser()
    parser.customization = convert_to_unicode
    bib_database = bibtexparser.load(bibtex_file, parser=parser)
    '''

    # list_acronym_country -> nom du pays en anglais majuscule , acronyme du pays       
    list_acronym_country = []

    resultfile = "result_{0}.txt".format(csvfile)
    with open('bibtex_csv_files/'+csvfile+".csv", 'w') as csvf:
        # create the csv writer
        csvwriter = csv.writer(csvf, delimiter=',')

        for entry in bib_database.entries :
            # initialize entries
            type_publi =""
            numero = ""
            language = "en"
            title = ""
            conf=""
            nb_pages=""
            volume = ""
            town=""
            country=""
            country_acr="XX"
            year = 0
            doi_value = ""
            publisher_book = ""
            editor_book = ""
            pubmed = ""

            # Recup type publi, numero, language
            ## ENTRYTYPE peut être : article, inproceedings, book, phdthesis

            if entry['ENTRYTYPE']=='article' : 
                cnt_article +=1
                type_publi = "ART"
                numero = "RI"+str(cnt_article)
                language = "fr"

            elif entry['ENTRYTYPE']=='inproceedings' : 
                cnt_inproceeding +=1
                type_publi = "COMM"
                numero = "CI"+str(cnt_inproceeding)
                language = "en"

            elif entry['ENTRYTYPE']=='book' : 
                cnt_book +=1
                type_publi = "OUV"
                numero = "O"+str(cnt_book)
                language = "en"

            # Recup title and format
            title = entry['title']
            title = title.replace("\n"," ")
            title = title.replace("\\emph","")

            # get authors according to source_bibtex
            listauthors = []
            try :
                #print ("AUTHOR:"+entry['author'])
                authors = entry['author']
                list_authors_mix = authors.split("\n")
                for auth in list_authors_mix :
                    lab_struct = ""
                    #print ("TEST name_user "+name_user+" - auth "+auth)

                    if name_user in auth :
                        #print ("Script_bibtex Match form_author DBLP")
                        auth = firstname_user+" "+name_user
                        lab_struct = labo_auth_final
                    if auth[-4:] == " and" :
                        auth = auth[:-4]
                    auth_full = auth.split(" ")
                    prenom = auth_full[0]
                    #prenom = prenom.encode('latin1')
                    nom = auth_full[-1]
                    #nom = nom.encode('latin1')
                    #print ("Script_dblp "+"author "+auth.encode('latin1')+ " - prenom "+ prenom.encode('latin1')+ " - nom "+nom.encode('latin1')+ str(type(auth.encode('latin1'))))
                    listauthors.append((nom,prenom,lab_struct))
            except KeyError :
                cnt_error_auth+=1          

            # Get journal for ARTICLES
            if type_publi == "ART" :
                try :
                    conf = entry['journal']
                    conf = conf.replace("\n"," ")
                except KeyError :
                    cnt_error_jrn+=1        
            
            # Get conf for COMM, split entry with ',' , first occurence is conf name
            # then search country from CSV in other occurences and if found, get previous occurence to get town (except USA previuous previous occurence)
            if type_publi == "COMM" :
                try :
                    booktitle = entry['booktitle']
                    conf_all = booktitle.split(",")
                    conf = conf_all[0]
                    conf = conf.replace("\n"," ")
                    conf = conf.replace("\\(^\\mboxth\\)","th")
                    conf = conf.replace("\\(^\\mboxe\\)","e")
                    prev_conf_elmt = ""
                    prev_prev_conf_elmt = ""
                    #print("TEST HERE conf_all {0}".format(conf_all))
                    for conf_elmt in conf_all :
                        conf_elmt = conf_elmt.strip()
                        conf_elmt = conf_elmt.upper()
                        #print("TEST HERE conf_elmt {0}".format(conf_elmt))
                        
                        try :
                            data_country = dict_countries[conf_elmt]
                            print("MATCH country conf_elmt {0}".format(conf_elmt))
                            if conf_elmt == "USA" or conf_elmt == "CANADA":
                                prev_prev_conf_elmt = prev_prev_conf_elmt.replace("\n"," ")
                                town = prev_prev_conf_elmt
                                country_acr = data_country
                                country = conf_elmt
                            elif conf_elmt == "LUXEMBOURG" or conf_elmt == "SINGAPORE":
                                country = conf_elmt
                                country_acr = data_country
                                town = country
                            else :                        
                                prev_conf_elmt = prev_conf_elmt.replace("\n"," ")
                                town = prev_conf_elmt
                                country_acr = data_country
                                country = conf_elmt
                                
                        except Exception as e :
                            #print("NO MATCH  conf_elmt {0}".format(conf_elmt))
                            pass
                    
                        prev_prev_conf_elmt = prev_conf_elmt
                        prev_conf_elmt = conf_elmt
                    
                    if country == "":
                        print("Virtual event")
                        #if 'Virtual Event' in conf_all :
                        if any("Virtual Event" in s for s in conf_all):
                            country='VIRTUAL EVENT'
                            country_acr = 'XX'
                        
                        #if 'Online Streaming' in conf_all :
                        elif any("Online" in s for s in conf_all):
                            country='ONLINE'
                            country_acr = 'XX'

                except KeyError :
                    cnt_error_booktitle+=1
            
            # get volume
            try :
                volume = entry['volume']
            except KeyError :
                cnt_error_vol+=1

            # get nb_pages
            try :
                nb_pages = entry['pages']
            except KeyError :
                cnt_error_pages+=1

            # get Year
            try :
                year = entry['year']
            except KeyError :
                cnt_error_year_art+=1

            # get DOI
            try :
                doi_value = entry['doi']
            except KeyError :
                cnt_error_doi+=1

            if (type_publi == "COMM") or (type_publi == "OUV") :
                # get Publisher
                try :
                    publisher_book = entry['publisher']
                except KeyError :
                    cnt_error_publisher+=1

                # get Editor
                try :
                    editor_book = entry['editor']
                except KeyError :
                    cnt_error_editor+=1


            #print("{0} {1} {2} {3} {4} {5} {6} {7}".format(numero, type_publi, title, listauthors, conf, year, town, country))
            print("")

            # Test value "single" 
            # if false -> call createXml_sendHal (create XML and deposit)
            # if true -> call create_single_xml (build XML without creation, no deposit)
            if (type_publi == "ART") or (type_publi == "COMM") or (type_publi == "OUV") :


                action_todo=""

                # Verification que la publi n'existe pas deja dans HAL
                # pour un meilleur matching, titre en minuscule et recherche par le champ title_t (sans casse ni accent)
                #title_low = title.lower()
                url_request = "https://api.archives-ouvertes.fr/search/?q=title_s:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(title)

                req = requests.get(url_request)       
                json = ""
                try :
                    json = req.json()
                except ValueError as ve :
                    print ("PROBLEME VALUEERROR {0}".format(ve))
                try : 
                    if json is not "" :
                        result = json['response']['docs']

                        # si un resultat est trouve, recup authors et URI pour affichage dans 'existe', action_todo = "E"
                        if (len(result) == 1 ) :
                            all_auth = ""
                            try :
                                tous_authors = result[0]["authFullName_s"]
                                for auth in tous_authors:
                                    all_auth = all_auth + auth+"-"
                            except KeyError as ke :
                                print ("error print authors existing publi keyerror {0}".format(ke))
                            existe = existe + "num. "+numero+" - "+result[0]["uri_s"]+" - auteurs:"+all_auth+"<br/>"
                            action_todo = "E"
                            cnt_depot_E+=1

                        # si plusieurs resultats trouves, recup URI pour affichage dans 'problemes_doublon', action_todo = "2"
                        if (len(result) >1 ) :
                            problemes_doublon = problemes_doublon + "num. "+numero+" - URLS "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"<br/>"
                            action_todo = "2"
                            cnt_depot_2+=1

                        # Si aucun resultat on peut deposer, action_todo = "D"
                        if (len(result) == 0 ) :
                            action_todo = "D"
                            result = False
                            accept_depot = True
                            a_deposer += "num. {0} au titre {1} peut etre deposee dans HAL<br/>".format(numero, title)#.decode("latin1") )

                            # Si caracteres incoherents (issus de DBLP) dans le titre -> pas de depot -> problemes_depot
                            if ("\\(^\\mbox" in title) :
                                print("-----------------> MBOX")
                                accept_depot = False
                                result = False

                            #title = title.encode("latin1")
                            #conf = conf.encode("latin1")
                            #town = town.encode("latin1")
                            #editor_book = editor_book.encode("latin1")

                            """ if bool_depot_preprod==False and bool_depot_prod==False :
                                if accept_depot == True :
                                    a_deposer += "num. {0} au titre {1} peut etre deposee dans HAL<br/>".format(numero, title.decode("latin1") )
                                    cnt_depot_A+=1
                                elif accept_depot == False :
                                    problemes_depot += "num. {0} au titre {1} -> pb ds le bibtex<br/>".format(numero,title.decode("latin1") )
                                    cnt_depot_P+=1

                            elif bool_depot_preprod==True or bool_depot_prod==True :
                                print ("bool_depot_prod ",bool_depot_prod )
                                if accept_depot == True :
                                    result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user, login_depot, passwd_depot, bool_depot_preprod, bool_depot_prod)
                                    print("RESULT REQUEST : http:{0} - text:{1}".format(result[0], result[1]))
                                    # methode createXml_sendHal renvoie true -> depot HAL ou preprod OK
                                    if (str(result[0]) == "200") or (str(result[0]) == "202") :
                                        #depots = depots + "num. "+numero+" au titre "+title+" deposee dans HAL<br/>"
                                        depots += "num. {0} au titre {1} deposee dans HAL<br/>".format(numero, title.decode("latin1") )
                                        cnt_depot_D+=1
                                    # methode createXml_sendHal renvoie true -> pb depot HAL ou preprod ou pas de depot demande    
                                    else :
                                        #problemes_depot = problemes_depot + "num. "+numero+" au titre "+title+" a un probleme de depot<br/>"
                                        problemes_depot += "num. {0} au titre {1} a un probleme de depot -> http:{2} - error:{3}<br/>".format(numero,title.decode("latin1") ,str(result[0], result[1]))
                                        cnt_depot_P+=1
                                elif accept_depot == False :
                                    problemes_depot += "num. {0} au titre {1} -> pb ds le bibtex<br/>".format(numero,title.decode("latin1") )
                                    cnt_depot_P+=1 """
                    else :
                        # pb de lecture json, pas depot -> problemes_url
                        action_todo = "P"
                        cnt_depot_P+=1
                        problemes_url = problemes_url + "num. "+numero+" au titre "+title+"<br/>"                        
                except KeyError :
                    # pb de lecture json, pas depot -> problemes_url
                    action_todo = "P"
                    cnt_depot_P+=1
                    problemes_url = problemes_url + "num. "+numero+" au titre "+title+"<br/>" 

                # Creation du CSV qui sera renvoye et affiche
                authors = authors.replace(" and",", ")
                authors = authors.replace("\r","")
                authors = authors.replace("\n","")
                list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))
            
            row = [numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, doi_value, editor_book, language]
            print(row)
            csvwriter.writerow(row)

            """ elif single == True :
                title = title.encode("latin1")
                conf = conf.encode("latin1")
                town = town.encode("latin1")
                editor_book = editor_book.encode("latin1")   
                login_user = login_depot
                reponse_single_xml = create_single_xml(listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr, doi_value, editor_book, volume, pubmed, name_user, labo_auth_final, id_hal_user, login_user) """

        # ------------------END LOOP -----------------
        cnt_total+=1

    ########################
    ####### ECRITURE RESULTATS  -> list_resultats
    print ("list_pub_csv length :",cnt_total)
    
    """ list_resultats=[]
    for pub in list_pub_csv :
        allauth = pub[1]
        allauth = allauth.replace("\n","")
        title = pub[2]#.decode("latin1") 
        conf = pub[3]#.decode("latin1") 
        ville = pub[9]#.decode("latin1")

        list_resultats.append((str(pub[0]),str(allauth),str(title),str(conf),str(pub[4]),str(pub[5]),str(pub[6]),str(pub[7]),str(pub[8]),ville,str(pub[10]),str(pub[11]),str(pub[12]))) """
    with open('bibtex_csv_files/'+resultfile, 'w') as resfile :
        #resfile.write = ""
        
        resfile.write("# RESULTS<br/>")
        resfile.write("## RESULTS PARSING BIBTEX<br/>")
        resfile.write("- cnt_article : {0}<br/>".format(cnt_article))
        resfile.write("- cnt_inproceeding : {0}<br/>".format(cnt_inproceeding))
        resfile.write("- cnt_proceeding : {0}<br/>".format(cnt_proceeding))
        resfile.write("- cnt_incollection : {0}<br/>".format(cnt_incollection))
        resfile.write("- cnt_book : {0}<br/>".format(cnt_book))
        resfile.write("- cnt_phdthesis : {0}<br/>".format(cnt_phdthesis))
        resfile.write("- cnt_total : {0}<br/>".format(cnt_total))
        resfile.write("<br/>")
        resfile.write("## RESULTS XML + DEPOSITS<br/>")
        resfile.write("**Existing deposits**<br/>")
        resfile.write(existe)
        resfile.write("<br/>")
        resfile.write("------------------<br/>")
        resfile.write("**To deposit**<br/>")
        resfile.write(a_deposer)
        resfile.write("<br/>")
        resfile.write("------------------<br/>")
        resfile.write("**Problems duplicates**<br/>")
        resfile.write(problemes_doublon)
        resfile.write("<br/>")
        resfile.write("------------------<br/>")
        resfile.write("**Problems URL**<br/>")
        resfile.write(problemes_url)
        resfile.write("<br/>")
        resfile.write("------------------<br/>")

        ## Creation tableau
        #mail_msg+="## Visualisation\n"
        #mail_msg+="\n"
        #mail_msg+="|num|Authors|Title|Journal-Conf|pp|vol|date|type|todo|Town|Country|\n"
        #for res in list_resultats :
        #    mail_msg+="|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|\n".format(res[0],res[1],res[2],res[3],res[4],res[5],res[6],res[7],res[8],res[9],res[10])

        resfile.write("<br/>")
        resfile.write("## ERRORS<br/>")
        resfile.write("- ERROR Author : {0}<br/>".format(cnt_error_auth))
        resfile.write("- ERROR Title : {0}<br/>".format(cnt_error_title))
        resfile.write("<br/>")
        resfile.write("**ERRORS ARTICLE**<br/>")
        resfile.write("- cnt_error_jrn : {0}<br/>".format(cnt_error_jrn))
        resfile.write("- cnt_error_vol : {0}<br/>".format(cnt_error_vol))
        resfile.write("- cnt_error_numb : {0}<br/>".format(cnt_error_numb))
        resfile.write("- cnt_error_pages : {0}<br/>".format(cnt_error_pages))
        resfile.write("- cnt_error_year_art : {0}<br/>".format(cnt_error_year_art))
        resfile.write("- cnt_error_doi : {0}<br/>".format(cnt_error_doi))
        resfile.write("<br/>")
        resfile.write("**ERRORS INPROCEEDINGS**<br/>")
        resfile.write("- cnt_error_booktitle : {0}<br/>".format(cnt_error_booktitle))
        resfile.write("- cnt_error_pages : {0}<br/>".format(cnt_error_pages))
        resfile.write("- cnt_error_year_inp : {0}<br/>".format(cnt_error_year_inp))
        resfile.write("- cnt_error_crossref : {0}<br/>".format(cnt_error_crossref))
        resfile.write("- cnt_error_publisher : {0}<br/>".format(cnt_error_publisher))
        resfile.write("- cnt_error_editor : {0}<br/>".format(cnt_error_editor))
        resfile.write("<br/>")
        resfile.write("**ERRORS PROCEEDINGS**<br/>")
        resfile.write("- cnt_error_idcrossref : {0}<br/>".format(cnt_error_idcrossref))
        resfile.write("<br/>")

    """ mail_subject = "[HALTOOLS] results"
    if bool_depot_prod == True :
        mail_subject+=" deposits PROD"
    elif bool_depot_preprod == True :
        mail_subject+=" deposits PREPROD"
    else :
        mail_subject+=" deposits TEST"

    mails.sendonemail(mail_subject,mail_msg, mail_reponse) """

    '''
    list_resultats.append(("RESULTATS","nombre de publis",str(cnt_nb_publis),"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","{0} publis deja presentes dans HAL".format(cnt_depot_E),resultat,"","","","","","","","","",""))
    list_resultats.append(("RESULTATS","{0} depots de publis".format(cnt_depot_D),depots,"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","{0} a déposer".format(cnt_depot_A),a_deposer,"","","","","","","","","","")) 
    list_resultats.append(("RESULTATS","{0} problemes de depot".format(cnt_depot_P),problemes_depot,"","","","","", "","","","",""))
    list_resultats.append(("RESULTATS","{0} problemes de doublon".format(cnt_depot_2),problemes_doublon,"","","","", "","","","","","")) 
    list_resultats.append(("RESULTATS","Problemes url",problemes_url,"","","","","","","","","","")) 
    '''
    
    print ("####### RESULTATS PARSING BIBTEX ########")
    print ("cnt_article {0}".format(cnt_article))
    print ("cnt_inproceeding {0}".format(cnt_inproceeding))
    print ("cnt_proceeding {0}".format(cnt_proceeding))
    print ("cnt_incollection {0}".format(cnt_incollection))
    print ("cnt_book {0}".format(cnt_book))
    print ("cnt_phdthesis {0}".format(cnt_phdthesis))
    print ("cnt_total {0}".format(cnt_total))

    print ("ERROR Author{0}".format(cnt_error_auth))
    print ("ERROR Title{0}".format(cnt_error_title))

    print ("-------ERRORS ARTICLE------")
    print ("cnt_error_jrn{0}".format(cnt_error_jrn))
    print ("cnt_error_vol{0}".format(cnt_error_vol))
    print ("cnt_error_numb{0}".format(cnt_error_numb))
    print ("cnt_error_pages{0}".format(cnt_error_pages))
    print ("cnt_error_year_art{0}".format(cnt_error_year_art))
    print ("cnt_error_doi{0}".format(cnt_error_doi))

    print ("-------ERRORS INPROCEEDINGS------")
    print ("cnt_error_booktitle:{0}".format(cnt_error_booktitle))
    print ("cnt_error_pages:{0}".format(cnt_error_pages))
    print ("cnt_error_year_inp:{0}".format(cnt_error_year_inp))
    print ("cnt_error_crossref:{0}".format(cnt_error_crossref))
    print ("cnt_error_publisher:{0}".format(cnt_error_publisher))
    print ("cnt_error_editor:{0}".format(cnt_error_editor))

    print ("-------ERRORS PROCEEDINGS------")
    print ("cnt_error_idcrossref:{0}".format(cnt_error_idcrossref))

    print ("#########################################")

    print ("######## RESULTATS XML + DEPOTS ##########")
    print ("RESULTATS existants")
    print (existe)#.encode("latin1"))
    print ("------------------")
    print ("A DEPOSER")
    print (a_deposer)#.encode("latin1"))
    print ("------------------")
    print ("PROBLEMES DOUBLONS")
    print (problemes_doublon)#.encode("latin1"))
    print ("------------------")
    print ("PROBLEMES URL")
    print (problemes_url)#.encode("latin1"))
    
    return None
    """ if single == False :
        print("SINGLE FALSE")
        return None
    if single == True :
        print("SINGLE TRUE")
        return reponse_single_xml """
