# README


## Presentation

Cette application permet aux référents HAL d'utiliser des outils d'imports de publications dans HAL

Ces outils reposent sur l'API SWORD de HAL.

Le principe de ces outils est de lire des listes de publications au format bibtex ou texte et de générer des fichiers XML (format TEI) qui seront envoyés sur l'API pour être importés dans la base de HAL.

Un rendu sous forme de tableau permet de visualiser les résultats et le choix est donné de déposer sur la base HAL ou sur une base de préproduction.

Dblp_2_Hal permet de faire des imports massifs dans HAL à partir de fichiers au format bibtex issus de DBLP.

Text_2_Hal permet de faire des imports massifs dans HAL à partir d'une liste texte, ou d'un CV de Research Gate. 


## Deployment


### Pip and virtualenv

Install Pip

```bash
sudo apt-get update
sudo apt-get install python-pip
pip install --upgrade pip
```

Install and set virtualenv

```bash
sudo apt install virtualenv
mkdir halenv
virtualenv halenv

source halenv/bin/activate
```


### Install MySql

Install MySql

```bash
sudo apt install mysql-server
sudo mysql_secure_installation
```

This last command finish the installation of MySql
Answers :
* N
* pwd root : ```*****```
* Y
* Y
* Y
* Y

```bash
sudo service mysql start
```

Access to console mysql :

```bash
sudo mysql -u root -p
```

Create database

```bash
create database haltools;
```

Before import data be sure to have UTF8 encoding.

```bash
ALTER DATABASE haltools charset=utf8;
```

```bash
CREATE USER 'haladmin'@'localhost' IDENTIFIED BY '******';
GRANT ALL PRIVILEGES ON haltools.* TO 'haladmin'@'localhost';
FLUSH PRIVILEGES;
```

### Install RabbitMQ

RabbitMQ est installé comme gestionnaire de queues pour permettre des requêtes asynchrones car trop longues

La librairie Celery est utilisée pour la gestion de ces requêtes

```
cd deploy
chmod 744 rabbit.sh
./rabbit.sh
sudo rabbitmqctl add_user rabbuser <password>
sudo rabbitmqctl set_permissions -p / rabbuser ".*" ".*" ".*"
cd ..
```

### Use Git

Install Git and configure

```bash
sudo apt-get install git

git config --global user.name "admin haltools"
git config --global user.email mymail@isima.fr
```

clone repository from gitlab

```bash
cd halenv
git clone https://gitlab.limos.fr/basdorea/haltools
```


### Dependencies

Manage MySql problems

```bash
sudo apt-get install libmysqlclient-dev
```

Install other dependencies with pip

```bash
cd haltools
pip install -r requirements.txt
```

### Settings

Modify file settings.py to access database, deploy upon a specific IP adress, ...

```bash
vim haltools/settings.py
> ALLOWED_HOSTS = ['10.0.0.***']
> DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS' : {"init_command": "SET foreign_key_checks = 0;"},
        'NAME': 'haltools',
        'USER': 'haladmin',
        'PASSWORD': '******',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}
```


### build from Django ORM

Fills the database with tables from django ORM models

```bash
python manage.py makemigrations
python manage.py migrate
```


Create a superuser Django

```bash
python manage.py createsuperuser
```

Launch server in local with remote access

```bash
cd haltools
python manage.py runserver 0.0.0.0:8000
```


browse http://127.0.0.1:8000/

browse http://127.0.0.1:8000/admin to authenticate with superuser

Launch celery process

```bash
celery -A haltools worker -l INFO --hostname 'rabbhaluser' --concurrency 2 -n w1
```
